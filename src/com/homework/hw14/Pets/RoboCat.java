package com.homework.hw14.Pets;

import com.homework.hw14.Constants.Species;

import java.util.Set;

public class RoboCat extends Pet implements MakingAFoul {

    static {
        System.out.println("New class "
                + RoboCat.class.getSimpleName()
                + " is loading...");
    }

    {

        System.out.println("New object " + getClass().getSimpleName() + " is creating...");

        setSpecies(Species.ROBOCAT);

    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {

        super(nickname, age, trickLevel, habits);

    }


    public void respond() {

        System.out.println("I am here, BOSS!. I am - " + getNickname() + "!");

    }

    @Override
    public void foul() {

        System.out.println("Two hundred twenty volts to your ass, Master!...");

    }


}
