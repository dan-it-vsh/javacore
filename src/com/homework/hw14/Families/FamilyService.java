package com.homework.hw14.Families;

import com.homework.hw14.DAO.CollectionFamilyDao;
import com.homework.hw14.DAO.FamilyDao;
import com.homework.hw14.Humans.Human;
import com.homework.hw14.Pets.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

public class FamilyService {

    private final String TIME_ZONE = "Europe/Kiev";

    private FamilyDao familyDao = new CollectionFamilyDao();

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        for (Family family : families) {
            System.out.println(family);
        }
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {

        List<Family> result = new ArrayList();

        List<Family> families = familyDao.getAllFamilies();

        for (Family family : families) {
            if (family.countFamilyMembers() > biggerThan) {

                System.out.println(family);
                result.add(family);
            }
        }

        return result;

    }

    public List<Family> getFamiliesLessThan(int lessThan) {

        List<Family> result = new ArrayList();

        List<Family> families = familyDao.getAllFamilies();

        for (Family family : families) {
            if (family.countFamilyMembers() < lessThan) {

                System.out.println(family);
                result.add(family);
            }
        }

        return result;

    }

    public int countFamiliesWithMemberNumber(int familyMembersNumber) {

        int result = 0;

        List<Family> families = familyDao.getAllFamilies();

        for (Family family : families) {
            if (family.countFamilyMembers() == familyMembersNumber) {

                result++;
            }
        }

        return result;

    }

    public void createNewFamily(Human father, Human mother) {

        Family family = familyDao.createFamily(father, mother);

        familyDao.saveFamily(family);

    }

    public void deleteFamilyByIndex(int index) {

        familyDao.deleteFamily(index);

    }

    public Family bornChild(Family family, String nameMens, String nameWomens) {

        family.bornChild(nameMens, nameWomens);
        familyDao.saveFamily(family);

        return family;

    }

    public Family adoptChild(Family family, Human newChild) {

        if (newChild != null)
            newChild.setSurname(family.getFather().getSurname());
        if (family != null)
            family.addChild(newChild);
        familyDao.saveFamily(family);

        return family;

    }

    public void deleteAllChildrenOlderThen(int olderThan) {

        List<Family> families = familyDao.getAllFamilies();
        Iterator<Family> iterFamilies = families.iterator();

        while (iterFamilies.hasNext()) {

            Family currFamily = iterFamilies.next();

            Iterator<Human> iterChildren = currFamily.getChildren().iterator();

            while (iterChildren.hasNext()) {

                Human currChild = iterChildren.next();

                if (isOlderThan(currChild, olderThan)) {

                    currChild.setFamily(null);
                    iterChildren.remove();

                }

            }

        }

    }

    private boolean isOlderThan(Human child, int olderThan) {

        boolean result = false;

        LocalDate dateNow = LocalDate.now(ZoneId.of(TIME_ZONE));                       //Today's instance
        Instant birthdayInstance = Instant.ofEpochMilli(child.getBirthDate());     //Birth date instance

        Period period = Period.between(
                birthdayInstance.atZone(ZoneId.of(TIME_ZONE)).toLocalDate(),
                dateNow
        );

        if (period.getYears() >= olderThan) result = true;

        return result;
    }

    public int count() {

        return familyDao.getAllFamilies().size();

    }

    public Family getFamilyById(int index) {

        if (index >= 0 && index < familyDao.getAllFamilies().size()) {

            return familyDao.getAllFamilies().get(index);

        } else {

            return null;

        }

    }

    public Set<Pet> getPets(int index) {

        if (index >= 0 && index < familyDao.getAllFamilies().size()) {

            return familyDao.getAllFamilies().get(index).getPets();

        } else {

            return null;

        }

    }

    public void addPet(int index, Pet pet) {

        if (index >= 0 && index < familyDao.getAllFamilies().size() && pet != null) {

            familyDao.getAllFamilies().get(index).getPets().add(pet);

        }

    }

}
