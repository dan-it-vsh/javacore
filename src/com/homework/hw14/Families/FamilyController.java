package com.homework.hw14.Families;

import com.homework.hw14.Humans.Human;
import com.homework.hw14.Pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {

        familyService.displayAllFamilies();

    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {

        return familyService.getFamiliesBiggerThan(biggerThan);

    }

    public List<Family> getFamiliesLessThan(int lessThan) {

        return familyService.getFamiliesLessThan(lessThan);

    }

    public int countFamiliesWithMemberNumber(int familyMembersNumber) {

        return familyService.countFamiliesWithMemberNumber(familyMembersNumber);

    }

    public void createNewFamily(Human father, Human mother) {

        familyService.createNewFamily(father, mother);

    }

    public void deleteFamilyByIndex(int index) {

        familyService.deleteFamilyByIndex(index);

    }

    public Family bornChild(Family family, String nameMens, String nameWomens) {

        return familyService.bornChild(family, nameMens, nameWomens);

    }

    public Family adoptChild(Family family, Human newChild) {

        return familyService.adoptChild(family, newChild);

    }

    public void deleteAllChildrenOlderThen(int olderThan) {

        familyService.deleteAllChildrenOlderThen(olderThan);

    }

    public int count() {

        return familyService.count();

    }

    public Family getFamilyById(int index) {

        return familyService.getFamilyById(index);

    }

    public Set<Pet> getPets(int index) {

        return familyService.getPets(index);

    }

    public void addPet(int index, Pet pet) {

        familyService.addPet(index, pet);

    }

}
