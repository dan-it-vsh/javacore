package com.homework.hw14.Humans;

public interface HumanCreator {

    Human bornChild(String nameMens, String nameWomens);

    public static String TIME_ZONE = "Europe/Kiev";

}
