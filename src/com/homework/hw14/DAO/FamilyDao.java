package com.homework.hw14.DAO;

import com.homework.hw14.Families.Family;
import com.homework.hw14.Humans.Human;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int Index);

    boolean deleteFamily(int Index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    Family createFamily(Human father, Human mother);

}
