package com.homework.hw11;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Dog myDog = new Dog("Sharik", 2, 20, new HashSet(Arrays.asList("eating", "sleepimg")));
        System.out.println(myDog);
        myDog.eat();
        myDog.respond();
        myDog.foul();

        RoboCat myRoboCat = new RoboCat("Murzik", 1, 90, new HashSet(Arrays.asList("eating", "sleeping", "running")));
        System.out.println(myRoboCat);
        myRoboCat.eat();
        myRoboCat.respond();
        myRoboCat.foul();

        Fish myFish = new Fish("Ponchik", 3, 30, new HashSet(Arrays.asList("swimming")));
        System.out.println(myFish);
        myFish.eat();
        myFish.respond();

        DomesticCat myDomCat = new DomesticCat("Vaska", 3, 30, new HashSet(Arrays.asList("lazy", "eating")));
        System.out.println(myDomCat);
        myDomCat.eat();
        myDomCat.respond();
        myDomCat.foul();

        Map<String, String> schedule1 = new HashMap<String, String>() {{

            put(DaysOfWeek.MONDAY.name(), "Monday's activity");
            put(DaysOfWeek.TUESDAY.name(), "Tuesday's activity");

        }};

        Map<String, String> schedule2 = new HashMap<String, String>() {{

            put(DaysOfWeek.SATURDAY.name(), "Saturday's activity");
            put(DaysOfWeek.SUNDAY.name(), "Sunday's activity");

        }};


        Man human1 = new Man("Petr", "Ivanov", 1980, 100, schedule1, null);
        Woman human2 = new Woman("Fekla", "Petrova", 1985, 110, schedule2, null);

        human1.greetPets();
        human1.repairCar();
        human2.greetPets();
        human2.makeup();

        Family myFamily1 = new Family(human2, human1);


        myFamily1.getPets().add(myDog);
        myFamily1.getPets().add(myFish);
        human1.greetPets();
        human2.greetPets();

        System.out.println(myFamily1);
        System.out.println(human1);
        System.out.println(human2);

        for (int i = 0; i < 5; i++) {

            Human child = myFamily1.bornChild();
            System.out.println(child);

            System.out.println(child.getFamily());
            System.out.println("Number of family members = " + child.getFamily().countFamily());

            child.greetPets();


        }

        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(10);
        System.out.println("Number of family members = " + myFamily1.countFamily());

        Random random = new Random();
        myFamily1.deleteChild(myFamily1.getChildren().get(random.nextInt(myFamily1.getChildren().size())));
        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(human1);
        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(0);
        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(0);
        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(human1);
        System.out.println("Number of family members = " + myFamily1.countFamily());

        myFamily1.deleteChild(0);
        System.out.println("Number of family members = " + myFamily1.countFamily());


        myFamily1.getFather().feedPets(true);
        myFamily1.getFather().feedPets(false);
        myFamily1.getFather().feedPets(false);
        myFamily1.getFather().feedPets(false);
        myFamily1.getFather().feedPets(false);

        myFamily1.getFather().greetPets();

    }


}
