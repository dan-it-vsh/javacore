package com.homework.hw11;

import java.util.Map;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int yearBirthDay;
    private int iqLevel;
    private Map<String,String> schedule;
    private Family family;

    static {
        System.out.println("New class "
                + Human.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor for all fields of the class Human
    public Human(String name, String surname, int yearBirthDay,
                 int iqLevel, Map<String,String> schedule,
                 Family family) {

        this.name = name;
        this.surname = surname;
        this.yearBirthDay = yearBirthDay;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
        this.family = family;

    }

    // START getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearBirthDay() {
        return yearBirthDay;
    }

    public void setYearBirthDay(int yearBirthDay) {
        this.yearBirthDay = yearBirthDay;
    }

    public int getIq() {
        return iqLevel;
    }

    public void setIq(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public Map<String,String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String,String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    // END getters and setters

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human myHuman = (Human) obj;
        return (name.equals(myHuman.getName())
                && surname.equals(myHuman.getSurname())
                && yearBirthDay == myHuman.getYearBirthDay()
        );
    }

    @Override
    public int hashCode() {
        int result = 11;
        int coef = 31;
        result = coef * result + name.hashCode();
        result = coef * result + surname.hashCode();
        result = coef * result + yearBirthDay;
        return result;
    }

    public void greetPets() {
        if (family != null && family.getPets() != null) {

            for (Pet myPet : family.getPets()) {

                System.out.println("Hello, " + myPet.getNickname() + "!");

            }
        } else
            System.out.println("Oh, I don't have a pet!");
    }

    public void describePets() {
        if (family != null && family.getPets() != null) {

            for (Pet myPet : family.getPets()) {

                String isTricky = "";
                if (myPet.getTrickLevel() <= 50) isTricky = "not ";
                System.out.println("I have a " + myPet.getSpecies() + ", it's " + myPet.getAge() +
                        " years old, it's " + isTricky + "very cunning!");

            }

        } else

            System.out.println("Oh, I don't have a pet! Nothing to describe!");

    }

    public boolean feedPets(boolean isTimeToFeed) {
        if (family.getPets() == null) return false;

        boolean feedingAtLeastOnce = false;

        if (isTimeToFeed) {

            for (Pet myPet : family.getPets()) {

                System.out.println("Feeding " + myPet.getNickname() + "!");

            }

            return true;

        } else {

            for (Pet myPet : family.getPets()) {

                Random random = new Random();

                if (myPet.getTrickLevel() > random.nextInt(101)) {

                    System.out.println("Let's feed " + myPet.getNickname() + "!");
                    feedingAtLeastOnce = true;

                } else {

                    System.out.println("I think " + myPet.getNickname() + " is not hungry!");

                }

            }
            return feedingAtLeastOnce;
        }
    }

    public String toString() {
        return "" + getClass().getSimpleName() + "{name=\'" + name + "\', surname=\'" + surname +
                "\', year=" + yearBirthDay + ", iq=" + iqLevel +
                ", schedule=" + schedule + "}";
    }


    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}
