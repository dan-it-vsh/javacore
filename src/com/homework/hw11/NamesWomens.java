package com.homework.hw11;

public enum NamesWomens {

    KATERINA("Katerina"),
    OLENA("Olena"),
    ALEKSANDRA("Aleksamdra"),
    LIUBOV("Liubov"),
    KHRYSTIA("Khrystia"),
    PARASKA("Paraska"),
    MELANKA("Melanka"),
    NADIA("Nadia"),
    OLGA("Olga"),
    GALYNA("Galyna");

    private final String name;

    NamesWomens(String nameWomens) {
        name = nameWomens;
    }

    public String getName() {
        return name;
    }

}
