package com.homework.lesson14HashMap;

public interface SimpleHashMap<K, V> {

    void put(K key, V value);
    V get(K key);
    boolean containsKey(K key);
    boolean containsValue(V value);

}
