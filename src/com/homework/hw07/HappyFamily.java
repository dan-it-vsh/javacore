package com.homework.hw07;

public class HappyFamily {
    public static void main(String[] args) {

        Pet puppy = new Pet("dog", "Puppy", 3, 77, new String[]{"eating", "sleeping"});
        System.out.println(puppy.toString());

        Human mother = new Human("Mariana", "Petrova", 1979);
        System.out.println(mother.toString());

        Human father = new Human("Vasia", "Petrov", 1978);
        System.out.println(father.toString());

        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework in java core.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "work; go to courses.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "work; do homework in java core.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "work; go to courses; watch a Champions league match.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "work; do java homework; watch a Europa league match.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "work; do java homework; preparation to java classes.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to major Java course.";

        Human boy = new Human("Porfirii", "Petrov", 2009);
        System.out.println(boy.toString());

        boy.setSchedule(schedule);
        System.out.println(boy.toString());

        Family family1 = new Family(mother, father);
        System.out.println(family1.toString());

        family1.setPet(puppy);

        Human girl = new Human("Fiokla", "Petrova", 2010);
        System.out.println(girl.toString());


        family1.addChild(girl);
        System.out.println(family1.toString());

        family1.addChild(boy);
        family1.addChild(boy);
        System.out.println(family1.toString());

        System.out.println("result of deleteChild is : "+ family1.deleteChild(girl));
        System.out.println(family1.toString());

        System.out.println("result of deleteChild is : "+ family1.deleteChild(boy));
        System.out.println(family1.toString());

        System.out.println("result of deleteChild is : "+ family1.deleteChild(girl));

        family1.addChild(new Human("Petia", "Petrov", 2011));
        System.out.println(family1.toString());

        family1.addChild(new Human("Matriona", "Petrova", 2011));
        System.out.println(family1.toString());

        family1.addChild(new Human("Matriona", "Petrova", 2011));
        System.out.println(family1.toString());

        System.out.println("result of deleteChild is : "+ family1.deleteChild(1));
        System.out.println("result of deleteChild is : "+ family1.deleteChild(1));
        System.out.println("result of deleteChild is : "+ family1.deleteChild(0));
        System.out.println(family1.toString());


    }
}
