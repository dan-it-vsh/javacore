package com.homework.hw06;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    // constructor for species and nickname fields of the class Pet only
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    // constructor for all fields of the class Pet
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    // empty constructor for class Pet
    public Pet() {
    }

    // START getters and setters

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    // END getters and setters

    public void eat() {
        System.out.println("I'm eating...");
    }

    public void respond() {
        System.out.println("Hello, boss!. I am - " + this.nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("It's needed to cover up my tracks...");
    }

    public String toString() {
        return species + "{nickname=\'" + nickname + "\', age=" + age +
                "," + " trickLevel=" + trickLevel + "," +
                " habits=" + Arrays.toString(habits) + "}";
    }

}
