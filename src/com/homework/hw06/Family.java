package com.homework.hw06;

import java.util.Arrays;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    // empty constructor
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

//    public void setMother(Human mother) {
//        this.mother = mother;
//    }

    public Human getFather() {
        return father;
    }

//    public void setFather(Human father) {
//        this.father = father;
//    }

    public Human[] getChildren() {
        return children;
    }

    public void addChild(Human child) {
//        if (childIsInChildren(child))
//            throw new Error("You're trying to add already existing-in-family child : " + child.toString());
        if (!childIsInChildren(child)) {
            if (child != null) {
                child.setFamily(this);

                Human[] newChildren = new Human[children.length + 1];

                for (int i = 0; i < children.length; i++) {
                    newChildren[i] = children[i];
                }

                newChildren[newChildren.length - 1] = child;
                children = newChildren;
            }
        }
    }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            for (int i = 0, j = 0; i < children.length; i++) {
                if (i != index)
                    newChildren[j++] = children[i];
                else children[i].setFamily(null);
            }
            children = newChildren;
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child) {
//        if (!childIsInChildren(child)) throw new Error("You're trying to delete un-existing-in-family child : " + child.toString());
        if (childIsInChildren(child)) {
            Human[] newChildren = new Human[children.length - 1];
            for (int i = 0, j = 0; i < children.length; i++) {
                if (!children[i].humanEquals(child))
                    newChildren[j++] = children[i];
                else children[i].setFamily(null);
            }
            children = newChildren;
            return true;
        } else
            return false;
    }

    private boolean childIsInChildren(Human child) {
        if (child == null) return false;
        for (int i = 0; i < children.length; i++)
            if (children[i].humanEquals(child)) return true;
        return false;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public int countFamily() {
        return 2 + children.length;
    }

    public String toString() {
        String motherFullName = null, fatherFullName = null;

        if (mother != null)
            motherFullName = mother.getName() + " " + mother.getSurname();

        if (father != null)
            fatherFullName = father.getName() + " " + father.getSurname();

        return "Family{mother=\'" + motherFullName + "\'" +
                " father=\'" + fatherFullName + "\'" +
                " children=\'" + Arrays.toString(getChildren()) + "\'" +
                " pet=\'" + pet + "\'}";
    }


}
