package com.homework.hw06;

public class HappyFamily {
    public static void main(String[] args) {

        System.out.println("Creating a pet....");
        Pet puppy = new Pet("dog","Puppy", 3, 77, new String[] { "eating", "sleeping" });
        System.out.println(puppy.toString());

        System.out.println("Creating mother....");
        Human mother = new Human("Mariana", "Petrova", 1979);
        System.out.println(mother.toString());
        System.out.println("mother's family is set to : " + mother.getFamily());

        System.out.println("Creating father....");
        Human father = new Human("Vasia", "Petrov", 1978);
        System.out.println(father.toString());
        System.out.println("father's family is set to : " + father.getFamily());


        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework in java core.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "work; go to courses.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "work; do homework in java core.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "work; go to courses; watch a Champions league match.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "work; do java homework; watch a Europa league match.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "work; do java homework; preparation to java classes.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to major Java course.";

        System.out.println("Creating boy....");
        Human boy = new Human("Porfirii", "Petrov", 2009);
        System.out.println(boy.toString());

        boy.setSchedule(schedule);
        System.out.println(boy.toString());

        System.out.println("Creating a family....");
        Family family1= new Family(mother,father);
        System.out.println(family1.toString());
        System.out.println("mother's family is set to : " + mother.getFamily());
        System.out.println("father's family is set to : " + father.getFamily());

        System.out.println("Assigning the pet to the family....");
        family1.setPet(puppy);

        System.out.println("Creating girl....");
        Human girl = new Human("Fiokla", "Petrova", 2010);
        System.out.println("child's family is set to : " + girl.getFamily());

        System.out.println("the family members number : " + family1.countFamily());
        System.out.println("Add a child to the family....");
        family1.addChild(girl);
        System.out.println(family1.toString());
        System.out.println("child's family is set to : " + girl.getFamily());
        System.out.println("the family members number : " + family1.countFamily());

        System.out.println("Add a child to the family....");
        family1.addChild(boy);
        System.out.println("the family members number : " + family1.countFamily());

        family1.addChild(boy);
        System.out.println("the family members number : " + family1.countFamily());

        System.out.println(family1.toString());
        System.out.println("mother's family is set to : " + mother.getFamily());
        System.out.println("father's family is set to : " + father.getFamily());
        System.out.println("child's family is set to : " + boy.getFamily());

        System.out.println("Delete the child from the family....");
        family1.deleteChild(boy);
        System.out.println("child's family is set to : " + boy.getFamily());

        family1.addChild(girl);
        family1.addChild(girl);
        System.out.println(girl.getFamily());
        System.out.println(family1.deleteChild(10));
        System.out.println(girl.getFamily());

        System.out.println(family1.deleteChild(0));
        System.out.println(family1.toString());
        System.out.println("the family members number : " + family1.countFamily());
        System.out.println("Delete the child from the family....");
//        family1.deleteChild(boy);
        System.out.println(family1.deleteChild(0));

        System.out.println(family1.toString());

        System.out.println("the family members number : " + family1.countFamily());
//        family1.deleteChild(girl);
        System.out.println(family1.deleteChild(0));
        System.out.println(family1.deleteChild(-1));
        System.out.println(family1.deleteChild(100));

        System.out.println("the family members number : " + family1.countFamily());

    }
}
