package com.homework.hw01;

import java.util.Scanner;
import java.util.Random;

public class Numbers {
    public static void main(String[] args) {
        // generating a random value
        Random random = new Random();
        int randNumber = random.nextInt(100) + 1;

//        System.out.print(randNumber);

        // entering user's name
        System.out.print("Enter your name : ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();

        // let's start message
        System.out.println("Let the game begin!");

        String inputString;
        int inputNumber;
        // array to store guessed numbers
        int[] guessNumbers = new int[200];
        int currentGuessNumberIndex = 0;

        while (true) {
            System.out.print("Guess a number : ");

            inputString = input.nextLine();

            if (isInteger(inputString)) {
                inputNumber = Integer.parseInt(inputString);
            } else {
                System.out.println("Your input is not a correct number!");
                continue;
            }

            guessNumbers[currentGuessNumberIndex++] = inputNumber;

            // if the number of attempts exceed the length of the array,
            // allocate new array
            // and copy values to the new one
            if (currentGuessNumberIndex >= guessNumbers.length)
                guessNumbers = newArrayAllocate(guessNumbers, currentGuessNumberIndex);


            if (inputNumber == randNumber) {
                System.out.println("Congratulations, " + name + "!");

                System.out.print("Your numbers: ");

                // sorting the guessNumbers array
                sortArray(guessNumbers, currentGuessNumberIndex);

                // printing all guessed numbers
                printArray(guessNumbers, currentGuessNumberIndex);

                break;

            } else if (inputNumber < randNumber) {

                System.out.println("Your number is too small. Please, try again.");

            } else {

                System.out.println("Your number is too big. Please, try again.");

            }
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false - is Integer
        return true;
    }

    private static void sortArray(int[] arr, int currentIndex) {
        for (int i = 0, tmp; i < currentIndex; i++)
            for (int j = i + 1; j < currentIndex; j++)
                if (arr[i] < arr[j]) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }

    }

    private static void printArray(int[] arr, int currentIndex) {
        for (int i = 0; i < currentIndex - 1; i++)
            System.out.print(arr[i] + ", ");

        System.out.println(arr[currentIndex - 1]);

    }

    private static int[] newArrayAllocate(int[] arr, int currentIndex) {
        int[] newArr = new int[arr.length+200];

        for (int i = 0; i < currentIndex; i++)
            newArr[i]=arr[i];

        return newArr;
    }

}
