package com.homework.hw01;

import java.util.Random;
import java.util.Scanner;

public class NumbersYearEvent {
    public static void main(String[] args) {
        String[][] eventArray = {
                {"1960", "Diego Maradona's birthday year"},
                {"1940", "Pele's birthday year"},
                {"1952", "Oleh Blokhin's birthday year"},
                {"1969", "Gabriel Batistuta's birthday year"},
                {"1992", "Neymar's birthday year"},
                {"1966", "Eric Cantona's birthday year"},
                {"1975", "David Beckham's birthday year"},
                {"1953", "Nigel Mansell's birthday year"},
                {"1987", "lionel Messi's birthday year"},
                {"1976", "Andriy Shevchenko's birthday year"},
        };

        // generating a random value
        Random random = new Random();
        int randNumber = random.nextInt(eventArray.length);

//        System.out.print(randNumber);

        // entering user's name
        System.out.print("Enter your name : ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();

        // let's start message
        System.out.println("Let the game begin!");

        String inputString;
        int inputNumber;
        // array to store guessed numbers
        int[] guessNumbers = new int[200];
        int currentGuessNumberIndex = 0;

        while (true) {
            System.out.print("Guess what is " + eventArray[randNumber][1] + " ? : ");
            inputString = input.nextLine();

            if (isInteger(inputString)) {
                inputNumber = Integer.parseInt(inputString);
            } else {
                System.out.println("Your input is not a correct year number (must be in format YYYY, only digits)!");
                continue;
            }

            guessNumbers[currentGuessNumberIndex++] = inputNumber;

            // if the number of attempts exceed the length of the array,
            // allocate new array
            // and copy values to the new one
            if (currentGuessNumberIndex >= guessNumbers.length)
                guessNumbers = newArrayAllocate(guessNumbers, currentGuessNumberIndex);

            if (inputNumber == Integer.parseInt(eventArray[randNumber][0])) {
                System.out.println("Congratulations, " + name + "!");

                System.out.print("Your year numbers were: ");

                // sorting the guessNumbers array
                sortArray(guessNumbers, currentGuessNumberIndex);

                // printing all guess numbers
                printArray(guessNumbers, currentGuessNumberIndex);

                break;

            } else if (inputNumber < Integer.parseInt(eventArray[randNumber][0])) {

                System.out.println("Your year number is too small. Please, try again.");

            } else {


                System.out.println("Your year number is too big. Please, try again.");
            }
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false - is Integer
        return true;
    }

    private static void sortArray(int[] arr, int currentIndex) {
        for (int i = 0, tmp; i < currentIndex; i++)
            for (int j = i + 1; j < currentIndex; j++)
                if (arr[i] < arr[j]) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }

    }

    private static void printArray(int[] arr, int currentIndex) {
        for (int i = 0; i < currentIndex - 1; i++)
            System.out.print(arr[i] + ", ");

        System.out.println(arr[currentIndex - 1]);

    }

    private static int[] newArrayAllocate(int[] arr, int currentIndex) {
        int[] newArr = new int[arr.length + 200];

        for (int i = 0; i < currentIndex; i++)
            newArr[i] = arr[i];

        return newArr;
    }

}
