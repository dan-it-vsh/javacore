package com.homework.hw12;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Family[] families = generateFourFamilies();

        CollectionFamilyDao familiesCollection = new CollectionFamilyDao();
        System.out.println(familiesCollection.getAllFamilies());

        for (Family family : families) {
            familiesCollection.saveFamily(family);
        }

        System.out.println(familiesCollection.getAllFamilies());

        System.out.println(familiesCollection.deleteFamily(10));
        System.out.println(familiesCollection.deleteFamily(0));
        System.out.println(familiesCollection.deleteFamily(families[3]));
        System.out.println(familiesCollection.deleteFamily(families[3]));
        System.out.println(familiesCollection.deleteFamily(0));
        System.out.println(familiesCollection.deleteFamily(0));
        System.out.println(familiesCollection.deleteFamily(0));

        System.out.println(familiesCollection.getAllFamilies());

        System.out.println(familiesCollection.numberOfFamilies());
        familiesCollection.saveFamily(families[2]);
        System.out.println(familiesCollection.numberOfFamilies());

        familiesCollection.saveFamily(families[2]);
        System.out.println(familiesCollection.numberOfFamilies());

        familiesCollection.saveFamily(families[3]);
        System.out.println(familiesCollection.numberOfFamilies());

        familiesCollection.saveFamily(families[0]);
        System.out.println(familiesCollection.numberOfFamilies());

        familiesCollection.saveFamily(families[0]);
        System.out.println(familiesCollection.numberOfFamilies());



    }


    public static Family[] generateFourFamilies() {
        Dog dog1 = new Dog("Sharik", 2, 20, new HashSet(Arrays.asList("eating", "sleepimg")));
        RoboCat robCat1 = new RoboCat("Murzik", 1, 90, new HashSet(Arrays.asList("eating", "sleeping", "running")));
        Fish fish1 = new Fish("Ponchik", 3, 30, new HashSet(Arrays.asList("swimming")));
        DomesticCat domCat1 = new DomesticCat("Vaska", 3, 30, new HashSet(Arrays.asList("lazy", "eating")));

        Man father1 = new Man("Petr", "Ivanov", 1976, 100, null, null);
        Man father2 = new Man("Ivan", "Sidorov", 1978, 107, null, null);
        Man father3 = new Man("Panas", "Petrov", 1981, 111, null, null);
        Man father4 = new Man("Chingiz", "Khanov", 1969, 114, null, null);
        Woman mother1 = new Woman("Fekla", "Ivanova", 1979, 99, null, null);
        Woman mother2 = new Woman("Paraska", "Sidorova", 1981, 113, null, null);
        Woman mother3 = new Woman("Marfa", "Petrova", 1983, 102, null, null);
        Woman mother4 = new Woman("Vama", "Khanova", 1975, 103, null, null);

        Family family1 = new Family(mother1, father1);
        Family family2 = new Family(mother2, father2);
        Family family3 = new Family(mother3, father3);
        Family family4 = new Family(mother4, father4);

        family1.getPets().add(dog1);
        family2.getPets().add(fish1);
        family3.getPets().add(domCat1);

        for (int i = 0; i < 3; i++) {

            family1.bornChild();
            family2.bornChild();
            family3.bornChild();
            family4.bornChild();

        }

        return new Family[] {family1, family2, family3, family4};

    }

}
