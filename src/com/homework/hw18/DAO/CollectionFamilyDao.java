package com.homework.hw18.DAO;

import com.homework.hw18.Families.Family;
import com.homework.hw18.Humans.Human;
import com.homework.hw18.Logger.FamilyLogger;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familiesList;
    private FamilyLogger logger;

    static {
        System.out.println("New class "
                + CollectionFamilyDao.class.getSimpleName()
                + " is loading...");
    }

    {

        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");

        String root = FileSystemView.getFileSystemView().getRoots()[0].getName();
        logger = new FamilyLogger(root + "tmp/app/log/application.log");

    }

    public CollectionFamilyDao() {

        familiesList = new ArrayList();

    }

    @Override
    public List<Family> getAllFamilies() {

        logger.info("Getting all families list...");

        return familiesList;

    }

    @Override
    public Family getFamilyByIndex(int index) {

        logger.info("Getting family by imdex = " + index + "...");

        Family result = null;

        if (index >= 0 && index < familiesList.size())
            result = familiesList.get(index);

        return result;
    }

    @Override
    public boolean deleteFamily(int index) {

        logger.info("Deleting family by imdex = " + index + "...");

        boolean result = false;

        if (index >= 0 && index < familiesList.size()) {
            familiesList.remove(index);
            result = true;
        }

        return result;

    }

    @Override
    public boolean deleteFamily(Family family) {

        logger.info("Deleting family by object = " + family + "...");

        return familiesList.remove(family);

    }

    @Override
    public void saveFamily(Family family) {

        logger.info("Saving information to db, family = " + family + "...");

        if (family != null) {

            if (familiesList.contains(family)) {

                familiesList.set(familiesList.indexOf(family), family);

            } else {

                familiesList.add(family);

            }
        }

    }

    @Override
    public Family createFamily(Human father, Human mother) {

        logger.info("Creating a new family = " + father + " + " + mother + "...");

        return new Family(father, mother);

    }


    public int numberOfFamilies() {

        logger.info("Returning the number of families in db...");

        return familiesList.size();

    }

    @Override
    public void saveData(String filePath) {

        logger.info("Saving families db to file...");

        try {

            if (new File(filePath).getParentFile().mkdirs())
                logger.info("Filepath created successfully.");
            FileOutputStream fileOutput = new FileOutputStream(filePath);
            ObjectOutputStream streamOutput = new ObjectOutputStream(fileOutput);
            streamOutput.writeObject(familiesList);
            streamOutput.close();
            fileOutput.close();

        } catch (IOException e) {

            logger.error(e.getMessage());

        }

    }

    @Override
    public void readData(String filePath) {

        logger.info("Reading families db from file...");

        List<Family> familiesListLoaded = null;

        try {

            FileInputStream fileInput = new FileInputStream(filePath);
            ObjectInputStream inputStream = new ObjectInputStream(fileInput);
            familiesListLoaded = (List<Family>) inputStream.readObject();
            inputStream.close();
            fileInput.close();
            loadData(familiesListLoaded);

        } catch (ClassNotFoundException | IOException e) {

            logger.error(e.getMessage());

        }

    }

    @Override
    public void loadData(List<Family> familiesList) {

        logger.info("Loading families info db...");

        if (familiesList != null)

            familiesList.forEach(this::saveFamily);

    }


}
