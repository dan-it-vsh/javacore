package com.homework.hw18.DAO;

import com.homework.hw18.Families.Family;
import com.homework.hw18.Humans.Human;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int Index);

    boolean deleteFamily(int Index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    Family createFamily(Human father, Human mother);

    void saveData(String filePath);

    void readData(String filePath);

    void loadData(List<Family> familyList);

}
