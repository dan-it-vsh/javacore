package com.homework.hw18.Families;

import com.homework.hw18.Exceptions.FamilyOverflowException;
import com.homework.hw18.Humans.Human;
import com.homework.hw18.Pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {

        familyService.displayAllFamilies();

    }

    public void saveData(String filePath) {

        familyService.saveData(filePath);

    }

    public void readData(String filePath) {

        familyService.readData(filePath);

    }


    public List<Family> getFamiliesBiggerThan(int biggerThan) {

        return familyService.getFamiliesBiggerThan(biggerThan);

    }

    public List<Family> getFamiliesLessThan(int lessThan) {

        return familyService.getFamiliesLessThan(lessThan);

    }

    public int countFamiliesWithMemberNumber(int familyMembersNumber) {

        return familyService.countFamiliesWithMembersNumber(familyMembersNumber);

    }

    public void createNewFamily(Human father, Human mother) {

        familyService.createNewFamily(father, mother);

    }

    public void deleteFamilyByIndex(int index) {

        familyService.deleteFamilyByIndex(index);

    }

    public Family bornChild(Family family, String nameMens, String nameWomens){

        Family result;

        try {

            result = familyService.bornChild(family, nameMens, nameWomens);

        } catch (FamilyOverflowException e) {

            e.printStackTrace();
            System.out.println("Error code = " + e.getErrorCode());
            result = family;

        }

        return result;

    }

    public Family adoptChild(Family family, Human newChild) {

        Family result;

        try {

            result = familyService.adoptChild(family, newChild);

        } catch (FamilyOverflowException e) {

            e.printStackTrace();
            System.out.println("Error code = " + e.getErrorCode());
            result = family;

        }

        return result;

    }

    public void deleteAllChildrenOlderThen(int olderThan) {

        familyService.deleteAllChildrenOlderThen(olderThan);

    }

    public int count() {

        return familyService.count();

    }

    public Family getFamilyById(int index) {

        return familyService.getFamilyById(index);

    }

    public Set<Pet> getPets(int index) {

        return familyService.getPets(index);

    }

    public void addPet(int index, Pet pet) {

        familyService.addPet(index, pet);

    }

}
