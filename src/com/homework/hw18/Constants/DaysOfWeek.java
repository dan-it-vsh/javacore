package com.homework.hw18.Constants;

public enum DaysOfWeek {

    MONDAY("MONDAY"),
    TUESDAY("TUESDAY"),
    WEDNESDAY("WEDNESDAY"),
    THURSDAY("THURSDAY"),
    FRIDAY("FRIDAY"),
    SATURDAY("SATURDAY"),
    SUNDAY("SUNDAY");

    private final String dayOfWeekName;

    private DaysOfWeek(String dayOfWeek) {
        dayOfWeekName = dayOfWeek;
    }

    public String getDayOfWeekName() {
        return dayOfWeekName;
    }

}
