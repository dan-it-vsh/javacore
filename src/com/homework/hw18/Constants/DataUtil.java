package com.homework.hw18.Constants;

public interface DataUtil {

    final String DATE_FORMAT = "dd/MM/yyyy";
    final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";  // for logger
    final String TIME_ZONE = "Europe/Kiev";

}
