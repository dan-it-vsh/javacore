package com.homework.hw18;

import com.homework.hw18.Constants.DataUtil;
import com.homework.hw18.Constants.DaysOfWeek;
import com.homework.hw18.Families.Family;
import com.homework.hw18.Families.FamilyController;
import com.homework.hw18.Humans.Human;
import com.homework.hw18.Pets.*;

import javax.swing.filechooser.FileSystemView;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class ConsoleApp implements DataUtil, Serializable {


    public void startApp() {

        FamilyController familiesDB = new FamilyController();
        String root = FileSystemView.getFileSystemView().getRoots()[0].getName();

        boolean control = true;

        while (control) {

            String message = "";
            int familyMembersNumber = 0;
            int familyIndex = 0;

            printMenuMain();

            Scanner input = new Scanner(System.in);
            System.out.print("Please enter your choice (1 - 12): ");
            int choice;

            try {

                choice = input.nextInt();

            } catch (InputMismatchException e) {

                choice = -1;

            }

            switch (choice) {
                case 1:
                    fillUpFamilyDatabaseAutomatically(familiesDB);

                    break;

                case 2:
                    System.out.println("Loading a list of families from file...");

                    String filePathIn = root + "tmp/app/db/db.txt";

                    familiesDB.readData(filePathIn);

                    break;

                case 3:
                    System.out.println("Saving the list of families to file...");

                    String filePathOut = root + "tmp/app/db/db.txt";

                    familiesDB.saveData(filePathOut);

                    break;

                case 4:
                    System.out.println("Displaying entire list of families...");

                    familiesDB.displayAllFamilies();

                    break;

                case 5:
                    message = "Please enter the number of family members : ";

                    familyMembersNumber = parseAndValidateInputInteger(message, 2, 20);

                    System.out.println("Families with the number of family members more than " + familyMembersNumber + ": ");
                    familiesDB.getFamiliesBiggerThan(familyMembersNumber);

                    break;

                case 6:
                    message = "Please enter the number of family members : ";

                    familyMembersNumber = parseAndValidateInputInteger(message, 2, 20);

                    System.out.println("Families with the number of family members less than " + familyMembersNumber + ": ");
                    familiesDB.getFamiliesLessThan(familyMembersNumber);

                    break;

                case 7:
                    message = "Please enter the number of family members : ";

                    familyMembersNumber = parseAndValidateInputInteger(message, 2, 20);

                    System.out.println("Families with the number of family members equals " + familyMembersNumber + ": " +
                            familiesDB.countFamiliesWithMemberNumber(familyMembersNumber));

                    break;

                case 8:
                    System.out.println("Creating a new family...");

                    familiesDB.createNewFamily(

                            generatingHuman("father"),
                            generatingHuman("mother")

                    );

                    break;

                case 9:
                    System.out.println("Deleting family by index...");

                    familyIndex = parseAndValidateInputInteger("Enter the family ordinal number (e.g. 1, 2, ...) to be deleted : ", 1, familiesDB.getAllFamilies().size()) - 1; // ordinal number is index + 1
                    familiesDB.deleteFamilyByIndex(familyIndex);

                    break;

                case 10:

                    boolean controlMenuEditFamily = true;

                    while (controlMenuEditFamily) {

                        printMenuEditFamily();

                        input = new Scanner(System.in);
                        System.out.print("Please enter your choice (1 - 3): ");
                        int choiceEditFamily;

                        try {

                            choiceEditFamily = input.nextInt();

                        } catch (InputMismatchException e) {

                            choiceEditFamily = -1;

                        }

                        switch (choiceEditFamily) {
                            case 1:

                                System.out.println("Bearing a child...");

                                familyIndex = parseAndValidateInputInteger("Enter the family ordinal number (e.g. 1, 2, ...) : ", 1, familiesDB.getAllFamilies().size()) - 1; // ordinal number is index + 1
                                String nameBoy = parseAndValidateInputString("Enter the name of the boy (if the boy is born) : ");
                                String nameGirl = parseAndValidateInputString("Enter the name of the girl (if the girl is born) : ");

                                familiesDB.bornChild(familiesDB.getFamilyById(familyIndex), nameBoy, nameGirl);

                                break;

                            case 2:

                                System.out.println("Adopting a child...");

                                familyIndex = parseAndValidateInputInteger("Enter the family ordinal number (e.g. 1, 2, ...) : ", 1, familiesDB.getAllFamilies().size()) - 1; // ordinal number is index + 1

                                familiesDB.adoptChild(familiesDB.getFamilyById(familyIndex),
                                        generatingHuman("child")
                                );

                                break;

                            case 3:

                                controlMenuEditFamily = false;

                                break;

                            default:
                                System.out.println("Your choice is wrong. Please repeat your choice.");

                        }
                    }

                    break;

                case 11:
                    System.out.println("Removing all children older than age...");
                    int age = parseAndValidateInputInteger("Enter the age of children : ", 0, 18);

                    familiesDB.deleteAllChildrenOlderThen(age);

                    break;

                case 12:
                    control = false;
                    break;

                default:
                    System.out.println("Your choice is wrong. Please repeat your choice.");

            }

        }

    }

    private static void printMenuMain() {

        System.out.println("1. Fill in with test data (automatically create several families and save them in the database).");
        System.out.println("2. Load a list of families from file.");
        System.out.println("3. Save the list of families to file.");
        System.out.println("4. Display the entire list of families (displays a list of all families with indexing starting with 1).");
        System.out.println("5. Display a list of families where the number of family members is more than the specified.");
        System.out.println("6. Display a list of families where the number of family members is less than the specified.");
        System.out.println("7. Count the number of families where the number of family members is.");
        System.out.println("8. Create a new family.");
        System.out.println("9. Delete family by family index in the general list.");
        System.out.println("10. Edit family by family index in the general list.");
        System.out.println("11. Remove all children older than age (in all families, children over the specified age are removed: they have grown).");
        System.out.println("12. Exit.");

    }

    private static void printMenuEditFamily() {

        System.out.println("1. Give a birth to a baby.");
        System.out.println("2. Adopt a child.");
        System.out.println("3. Return to the main menu.");

    }


    private void fillUpFamilyDatabaseAutomatically(FamilyController familiesDB) {
        System.out.println("Generating database of families...");

        Map<String, String> schedule1 = new HashMap() {{

            put(DaysOfWeek.MONDAY.name(), "Monday's activity");
            put(DaysOfWeek.TUESDAY.name(), "Tuesday's activity");

        }};

        Map<String, String> schedule2 = new HashMap() {{

            put(DaysOfWeek.SATURDAY.name(), "Saturday's activity");
            put(DaysOfWeek.SUNDAY.name(), "Sunday's activity");

        }};

        familiesDB.createNewFamily(
                new Human("Petr", "Ivanov", 205448401976L, 100, schedule1, null),
                new Human("Fekla", "Ivanova", 289342801979L, 99, null, null)
        );
        familiesDB.createNewFamily(
                new Human("Ivan", "Sidorov", 259189201978L, 107, null, null),
                new Human("Paraska", "Sidorova", 375656401981L, 113, null, null)
        );
        familiesDB.createNewFamily(
                new Human("Panas", "Petrov", 363038401981L, 111, schedule1, null),
                new Human("Marfa", "Petrova", 422049601983L, 102, null, null)
        );
        familiesDB.createNewFamily(
                new Human("Chingiz", "Khanov", -31373998031L, 114, schedule2, null),
                new Human("Vama", "Khanova", 182638801975L, 103, null, null)
        );
        familiesDB.createNewFamily(
                new Human("Ivanko", "Paramonov", 259189201978L, 121, schedule1, null),
                new Human("Marusia", "Paramonova", 422049601983L, 111, null, null)
        );

        Family family1 = familiesDB.getFamilyById(0);
        Family family2 = familiesDB.getFamilyById(1);
        Family family3 = familiesDB.getFamilyById(2);
        Family family4 = familiesDB.getFamilyById(3);
        Family family5 = familiesDB.getFamilyById(3);

        familiesDB.bornChild(family1, "Vania", "Masha");
        familiesDB.bornChild(family1, "Pasha", "Ira");
        familiesDB.adoptChild(family1, new Human("Cheburek", "Aitmatov", "12/12/2011", 100));
        familiesDB.addPet(0, new Dog("Sharik", 3, 35, new HashSet(Arrays.asList("playing", "fouling"))));

        familiesDB.bornChild(family2, "Sasha", "Marusia");
        familiesDB.addPet(1, new Dog("Sharik", 2, 20, new HashSet(Arrays.asList("eating", "sleepimg"))));

        familiesDB.bornChild(family3, "Vova", "Lena");
        familiesDB.addPet(2, new RoboCat("Murzik", 1, 90, new HashSet(Arrays.asList("eating", "sleeping", "running"))));

        familiesDB.bornChild(family4, "Oleg", "Natasha");
        familiesDB.bornChild(family4, "Valera", "Sasha");
        familiesDB.bornChild(family4, "Pavel", "Marina");
        familiesDB.addPet(3, new Fish("Ponchik", 3, 30, new HashSet(Arrays.asList("swimming"))));
        familiesDB.addPet(3, new DomesticCat("Vaska", 3, 30, new HashSet(Arrays.asList("lazy", "eating"))));

    }

    private int parseAndValidateInputInteger(String message, int startRange, int endRange) {

        int result = 0;
        boolean control = true;

        System.out.print(message);

        while (control) {
            Scanner input = new Scanner(System.in);

            try {

                result = input.nextInt();

            } catch (InputMismatchException e) {

                System.out.print("You entered incorrect type. ");
                result = -1;

            }

            if (result >= startRange && result <= endRange) control = false;
            else System.out.print("Enter correct number between " +
                    startRange + " and " + endRange + " : ");

        }
        return result;
    }

    private String parseAndValidateInputString(String message) {

        String result = "";
        String textPattern = "^[A-Z][a-z]+";
        boolean control = true;

        System.out.print(message);

        Scanner input = new Scanner(System.in);

        while (control) {

            result = input.nextLine().trim();

            if (result.trim().matches(textPattern)) control = false;
            else System.out.print("Enter correct String value with pattern " + textPattern + " (e.g. Vasia): ");

        }
        return result;
    }

    private Human generatingHuman(String nickname) {

        String name = parseAndValidateInputString("Enter " + nickname + "\'s name : ");
        String surname = parseAndValidateInputString("Enter " + nickname + "\'s surname : ");

        int yearBirthDateMin;
        int yearBirthDateMax;

        if (nickname.contains("father") || nickname.contains("mother")) {
            // the age must be more than 17 year for father and mother to start a family
            yearBirthDateMin = LocalDate.now(ZoneId.of(TIME_ZONE)).getYear() - 120; // people don't live so long
            yearBirthDateMax = LocalDate.now(ZoneId.of(TIME_ZONE)).getYear() - 17;

        } else {
            // the age of adopting child could be any
            yearBirthDateMin = LocalDate.now(ZoneId.of(TIME_ZONE)).getYear() - 120; // people don't live so long
            yearBirthDateMax = LocalDate.now(ZoneId.of(TIME_ZONE)).getYear();

        }

        int yearBirthDate = parseAndValidateInputInteger("Enter the year of " + nickname + "\'s birthdate : ", yearBirthDateMin, yearBirthDateMax);
        int monthBirthDate = parseAndValidateInputInteger("Enter the month of " + nickname + "\'s birthdate : ", 1, 12);
        int dayBirthDate = parseAndValidateInputInteger("Enter the day of " + nickname + "\'s birthdate : ", 1, LocalDate.of(yearBirthDate, monthBirthDate, 1).with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth());

        int iq = parseAndValidateInputInteger("Enter " + nickname + "\'s iq : ", 0, 200);

        return new Human(

                name,
                surname,
                LocalDate.of(yearBirthDate, monthBirthDate, dayBirthDate).format(DateTimeFormatter.ofPattern(DATE_FORMAT)),
                iq);

    }


}