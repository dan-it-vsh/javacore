package com.homework.hw18.Exceptions;

public class FamilyOverflowException extends RuntimeException {

    private String errorCode = "Family members count is more than FIVE persons";

    public FamilyOverflowException (String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return this.errorCode;
    }


}
