package com.homework.hw18.Humans;

import com.homework.hw18.Constants.DataUtil;

public interface HumanCreator {

    Human bornChild(String nameMens, String nameWomens);

}
