package com.homework.hw08;

import sun.reflect.generics.tree.SimpleClassTypeSignature;

import java.util.Arrays;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int yearBirthDay;
    private int iqLevel;
    private String[][] schedule;
    private Family family;

    static {
        System.out.println("New class "
                + Human.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor for name. surname, yearBirthDay fields
    // of the class Human only
    public Human(String name, String surname, int yearBirthDay) {
        this.name = name;
        this.surname = surname;
        this.yearBirthDay = yearBirthDay;
    }

    // constructor for all fields of the class Human
    public Human(String name, String surname, int yearBirthDay,
                 int iqLevel, String[][] schedule,
                 Family family) {
        this(name, surname, yearBirthDay);
        this.iqLevel = iqLevel;
        this.schedule = schedule;
        this.family = family;
    }

    // START getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearBirthDay() {
        return yearBirthDay;
    }

    public void setYearBirthDay(int yearBirthDay) {
        this.yearBirthDay = yearBirthDay;
    }

    public int getIq() {
        return iqLevel;
    }

    public void setIq(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    // END getters and setters

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human myHuman = (Human) obj;
        return (name.equals(myHuman.getName())
                && surname.equals(myHuman.getSurname())
                && yearBirthDay == myHuman.getYearBirthDay()
        );
    }

    @Override
    public int hashCode() {
        int result = 11;
        int coef = 31;
        result = coef * result + name.hashCode();
        result = coef * result + surname.hashCode();
        result = coef * result + yearBirthDay;
        return result;
    }

    public void greetPet() {
        if (family.getPet() != null)
            System.out.println("Hello, " + family.getPet().getNickname() + "!");
        else
            System.out.println("Oh, I don't have a pet!");
    }

    public void describePet() {
        if (family.getPet() != null) {
            String isTricky = "";
            if (family.getPet().getTrickLevel() <= 50) isTricky = "not ";
            System.out.println("I have a " + family.getPet().getSpecies() + ", it's " + family.getPet().getAge() +
                    " years old, it's " + isTricky + "very cunning!");
        } else
            System.out.println("Oh, I don't have a pet! Nothing to describe!");
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (family.getPet() == null) return false;

        if (isTimeToFeed) {
            System.out.println("Feeding " + family.getPet().getNickname() + "!");
            return true;
        } else {
            Random random = new Random();
            int randTrickLevel = random.nextInt(101);
            if (family.getPet().getTrickLevel() > randTrickLevel) {
                System.out.println("Let's feed " + family.getPet().getNickname() + "!");
                return true;
            } else {
                System.out.println("I think " + family.getPet().getNickname() + " is not hungry!");
                return false;
            }
        }
    }

    public String toString() {
        return "Human{name=\'" + name + "\', surname=\'" + surname +
                "\', year=" + yearBirthDay + ", iq=" + iqLevel +
                ", schedule=" + Arrays.deepToString(schedule) + "}";
    }

    public void printSchedule(String[][] schedule) {
        if (schedule != null) {
            for (int i = 0; i < schedule.length; i++) {
                for (int j = 0; j < schedule[0].length; j++)
                    System.out.printf("%10s : ", schedule[i][j]);
                System.out.println();
            }

        }
    }

    protected void finalize ( ) {
        System.out.println("Deleting object " + toString());
    }

}
