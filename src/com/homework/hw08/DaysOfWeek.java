package com.homework.hw08;

public enum DaysOfWeek {

    MONDAY("MONDAY"),
    TUESDAY("TUESDAY"),
    WEDNESDAY("WEDNESDAY"),
    THURSDAY("THURSDAY"),
    FRIDAY("FRIDAY"),
    SATURDAY("SATURDAY"),
    SUNDAY("SUNDAY");

    private final String dayOfWeekName;

    private DaysOfWeek(String dayOfWeek) {
        dayOfWeekName = dayOfWeek;
    }

    public String toString() {
        return dayOfWeekName;
    }

}
