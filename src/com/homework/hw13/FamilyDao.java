package com.homework.hw13;

import java.util.List;

interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int Index);

    boolean deleteFamily(int Index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    Family createFamily(Human father, Human mother);

}
