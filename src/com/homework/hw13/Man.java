package com.homework.hw13;

import java.util.Map;

public final class Man extends Human {

    static {

        System.out.println("New class "
                + Man.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");

    }

    public Man(String name, String surname, int yearBirthDay,
               int iqLevel, Map<String,String> schedule,
               Family family) {

        super(name, surname, yearBirthDay, iqLevel, schedule, family);

    }

    @Override
    public void greetPets() {
        if (getFamily() != null && getFamily().getPets() != null)

            for (Pet myPet : getFamily().getPets()) {
                System.out.println("Go walking, " + myPet.getNickname() + "!");
            }

        else
            System.out.println("Damn, I don't have a pet!");
    }

    public void repairCar() {

        System.out.println("Let's go, " + getName() + ", to meet the car!");

    }

}
