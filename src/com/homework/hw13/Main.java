package com.homework.hw13;

import java.util.Arrays;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {

        FamilyController controller = new FamilyController();

        System.out.println(controller.getAllFamilies());

        controller.displayAllFamilies();

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", 1975, 110, null, null),
                new Human("Paraska", "Pupkina", 1977, 105, null, null)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", 1975, 110, null, null),
                new Human("Paraska", "Pupkina", 1977, 105, null, null)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", 1975, 110, null, null),
                new Human("Paraska", "Pupkina", 1977, 105, null, null)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", 1975, 110, null, null),
                new Human("Fekla", "Pupkina", 1977, 105, null, null)
        );

        controller.displayAllFamilies();

        System.out.println(controller.getAllFamilies());

        controller.getFamiliesBiggerThan(2);
        controller.getFamiliesLessThan(2);

        System.out.println(controller.countFamiliesWithMemberNumber(0));
        System.out.println(controller.countFamiliesWithMemberNumber(1));
        System.out.println(controller.countFamiliesWithMemberNumber(2));

        controller.deleteFamilyByIndex(0);
        System.out.println(controller.countFamiliesWithMemberNumber(2));


        controller.displayAllFamilies();
        controller.bornChild(controller.getAllFamilies().get(0), "Petia", "Masha");
        controller.bornChild(controller.getAllFamilies().get(0), "", "");
        controller.bornChild(controller.getAllFamilies().get(0), "", "");
        controller.displayAllFamilies();

        controller.adoptChild(controller.getAllFamilies().get(0), new Human("Vasia", "DDDDD", 2011, 101, null, null));
        controller.displayAllFamilies();
        controller.adoptChild(controller.getAllFamilies().get(0), null);
        controller.adoptChild(null, null);
        controller.displayAllFamilies();


        controller.deleteAllChildrenOlderThen(8);
        controller.displayAllFamilies();

        controller.deleteAllChildrenOlderThen(5);
        controller.displayAllFamilies();

        System.out.println("number of families = " + controller.count());

        int index1 = 1;
        System.out.println("Family by index " + index1 + " = " + controller.getFamilyById(index1));

        index1 = 0;
        System.out.println("Family by index " + index1 + " = " + controller.getFamilyById(index1));

        System.out.println("Pets list in family by index " + index1 + " = " + controller.getPets(index1));

        controller.addPet(index1, new Dog("matroskin", 5, 55, new HashSet(Arrays.asList("sleeping","playing"))));

        System.out.println("Pets list in family by index " + index1 + " = " + controller.getPets(index1));

        controller.displayAllFamilies();

        controller.addPet(index1, null);

        controller.displayAllFamilies();

    }

}
