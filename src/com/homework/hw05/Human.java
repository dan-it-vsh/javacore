package com.homework.hw05;

import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int yearBirthDay;
    private int iqLevel;
    private String[][] schedule;
    private Pet pet;
    private Human mother;
    private Human father;

    // constructor for name. surname, yearBirthDay fields
    // of the class Human only
    public Human(String name, String surname, int yearBirthDay) {
        this.name = name;
        this.surname = surname;
        this.yearBirthDay = yearBirthDay;
    }

    // constructor for name. surname, yearBirthDay, mother, father  fields
    // of the class Human only
    public Human(String name, String surname, int yearBirthDay,
                 Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.yearBirthDay = yearBirthDay;
        this.mother = mother;
        this.father = father;
    }

    // constructor for all fields of the class Human only
    public Human(String name, String surname, int yearBirthDay,
                 int iqLevel, String[][] schedule,
                 Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.yearBirthDay = yearBirthDay;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
    }

    // empty constructor of the class Human
    public Human() {
    }

    public void greetPet() {
        if (pet != null)
            System.out.println("Hello, " + pet.getNickname() + "!");
        else
            System.out.println("Oh, I don't have a pet!");
    }

    public void describePet() {
        if (pet != null) {
            String isTricky = "";
            if (pet.getTrickLevel() <= 50) isTricky = "not ";
            System.out.println("I have a " + pet.getSpecies() + ", it's " + pet.getAge() +
                    " years old, it's " + isTricky + "very cunning!");
        } else
            System.out.println("Oh, I don't have a pet! Nothing to describe!");
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (this.pet == null) return false;

        if (isTimeToFeed) {
            System.out.println("Feeding " + this.pet.getNickname() + "!");
            return true;
        } else {
            Random random = new Random();
            int randTrickLevel = random.nextInt(101);
            if (this.pet.getTrickLevel() > randTrickLevel) {
                System.out.println("Let's feed " + this.pet.getNickname() + "!");
                return true;
            } else {
                System.out.println("I think " + this.pet.getNickname() + " is not hungry!");
                return false;
            }
        }
    }

    public void setIq(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void addMother(Human mother) {
        this.mother = mother;
    }

    public void addFather(Human father) {
        this.father = father;
    }

    public void addPet(Pet pet) {
        this.pet = pet;
    }

    public String toString() {
        String mother, father, pet;

        if (this.mother != null)
            mother = this.mother.getName() + " " + this.mother.getSurname();
        else
            mother = "mother is unknown!";

        if (this.father != null)
            father = this.father.getName() + " " + this.father.getSurname();
        else
            father = "father is unknown!";

        if (this.pet != null)
            pet = this.pet.toString();
        else
            pet = "is not bought yet!";

        return "Human{name=\'" + this.name + "\', surname=\'" + this.surname +
                "\', year=" + this.yearBirthDay + ", iq=" + this.iqLevel +
                " mother=\'" + mother + "\'" +
                " father='" + father + "\'" +
                " pet=\'" + pet + "\'}";
    }

    public void printSchedule(String[][] schedule) {
        if (schedule != null) {
            for (int i = 0; i < schedule.length; i++) {
                for (int j = 0; j < schedule[0].length; j++)
                    System.out.printf("%10s : ", schedule[i][j]);
                System.out.println();
            }

        }
    }

}
