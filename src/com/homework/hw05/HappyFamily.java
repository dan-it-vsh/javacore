package com.homework.hw05;

public class HappyFamily {
    public static void main(String[] args) {

        Pet tom = new Pet();
        System.out.println(tom.toString());

        Pet jerry = new Pet("mouse","Jerry");
        System.out.println(jerry.toString());

        Pet puppy = new Pet("dog","Puppy", 3, 77, new String[] { "eating", "sleeping" });
        System.out.println(puppy.toString());

        Human incognito = new Human();
        System.out.println(incognito.toString());

        Human grandpa1 = new Human("Ivan", "Ivanov", 1943);
        System.out.println(grandpa1.toString());

        Human grandma1 = new Human("Marfa", "Ivanova", 1944);
        System.out.println(grandma1.toString());

        Human grandpa2 = new Human("Petr", "Petrov", 1942);
        System.out.println(grandpa2.toString());

        Human grandma2 = new Human("Khrystia", "Petrova", 1948);
        System.out.println(grandma2.toString());

        Human mother = new Human("Mariana", "Petrova", 1979, grandma1, grandpa1);
        System.out.println(mother.toString());

        Human father = new Human("Vasia", "Petrov", 1978, grandma2, grandpa2);
        System.out.println(father.toString());

        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework in java core.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "work; go to courses.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "work; do homework in java core.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "work; go to courses; watch a Champions league match.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "work; do java homework; watch a Europa league match.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "work; do java homework; preparation to java classes.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to major Java course.";

        Human boy = new Human("Porfirii", "Petrov", 2009, 110, schedule, puppy, mother, father);
        System.out.println(boy.toString());
        boy.printSchedule(schedule);
    }
}
