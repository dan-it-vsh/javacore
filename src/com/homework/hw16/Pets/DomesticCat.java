package com.homework.hw16.Pets;

import com.homework.hw16.Constants.Species;

import java.util.Set;

public class DomesticCat extends Pet implements MakingAFoul {

    static {

        System.out.println("New class "
                + DomesticCat.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + getClass().getSimpleName() + " is creating...");

        setSpecies(Species.DOMESTICCAT);

    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {

        super(nickname, age, trickLevel, habits);

    }

    public void respond() {

        System.out.println("What do you want, looser!. I am - " + getNickname() + "!");

    }

    @Override
    public void foul() {

        System.out.println("It's needed to cover up my tracks...");

    }


}
