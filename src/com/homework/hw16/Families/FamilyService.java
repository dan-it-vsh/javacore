package com.homework.hw16.Families;

import com.homework.hw16.Constants.DataUtil;
import com.homework.hw16.DAO.CollectionFamilyDao;
import com.homework.hw16.DAO.FamilyDao;
import com.homework.hw16.Humans.Human;
import com.homework.hw16.Pets.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService implements DataUtil {

    private FamilyDao familyDao = new CollectionFamilyDao();

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {

        AtomicInteger counter = new AtomicInteger(1);
        familyDao.getAllFamilies().forEach(item->System.out.println(item.prettyFormat().replaceAll("^family:", "Family ID=" + counter.getAndIncrement() + ":")));
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {

        List<Family> result = new ArrayList();

        familyDao.getAllFamilies().stream()
                .filter(item -> (item.countFamilyMembers() > biggerThan))
                .forEach(item -> {
                    result.add(item);
                    System.out.println(item.prettyFormat());
                });

        return result;

    }

    public List<Family> getFamiliesLessThan(int lessThan) {

        List<Family> result = new ArrayList();

        familyDao.getAllFamilies().stream()
                .filter(item -> (item.countFamilyMembers() < lessThan))
                .forEach(item -> {
                    result.add(item);
                    System.out.println(item.prettyFormat());
                });

        return result;

    }

    public int countFamiliesWithMemberNumber(int familyMembersNumber) {

        return (int) familyDao.getAllFamilies().stream()
                .filter(item -> (item.countFamilyMembers() == familyMembersNumber))
                .count();

    }

    public void createNewFamily(Human father, Human mother) {

        Family family = familyDao.createFamily(father, mother);

        familyDao.saveFamily(family);

    }

    public void deleteFamilyByIndex(int index) {

        familyDao.deleteFamily(index);

    }

    public Family bornChild(Family family, String nameMens, String nameWomens) {

        family.bornChild(nameMens, nameWomens);
        familyDao.saveFamily(family);

        return family;

    }

    public Family adoptChild(Family family, Human newChild) {

        if (newChild != null)
            newChild.setSurname(family.getFather().getSurname());
        if (family != null)
            family.addChild(newChild);
        familyDao.saveFamily(family);

        return family;

    }

    public void deleteAllChildrenOlderThen(int olderThan) {

        familyDao.getAllFamilies()
                .forEach(itemFamily -> itemFamily.getChildren()
                        .removeIf(itemChild -> isOlderThan(itemChild, olderThan)));

    }

    private boolean isOlderThan(Human child, int olderThan) {

        boolean result = false;

        LocalDate dateNow = LocalDate.now(ZoneId.of(TIME_ZONE));                       //Today's instance
        Instant birthdayInstance = Instant.ofEpochMilli(child.getBirthDate());     //Birth date instance

        Period period = Period.between(
                birthdayInstance.atZone(ZoneId.of(TIME_ZONE)).toLocalDate(),
                dateNow
        );

        if (period.getYears() >= olderThan) result = true;

        return result;
    }

    public int count() {

        return familyDao.getAllFamilies().size();

    }

    public Family getFamilyById(int index) {

        if (index >= 0 && index < familyDao.getAllFamilies().size()) {

            return familyDao.getAllFamilies().get(index);

        } else {

            return null;

        }

    }

    public Set<Pet> getPets(int index) {

        if (index >= 0 && index < familyDao.getAllFamilies().size()) {

            return familyDao.getAllFamilies().get(index).getPets();

        } else {

            return null;

        }

    }

    public void addPet(int index, Pet pet) {

        if (index >= 0 && index < familyDao.getAllFamilies().size() && pet != null) {

            familyDao.getAllFamilies().get(index).getPets().add(pet);

        }

    }

}
