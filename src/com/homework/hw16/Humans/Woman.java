package com.homework.hw16.Humans;

import com.homework.hw16.Families.Family;
import com.homework.hw16.Pets.Pet;

import java.util.Map;

public final class Woman extends Human {

    static {

        System.out.println("New class "
                + Woman.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");

    }

    public Woman(String name, String surname, long birthDate,
          int iqLevel, Map<String,String> schedule,
          Family family) {

        super(name, surname, birthDate, iqLevel, schedule, family);

    }

    @Override
    public void greetPets() {
        if (getFamily() != null && getFamily().getPets() != null)

            for (Pet myPet : getFamily().getPets()) {

                System.out.println("Hello, my cute " + myPet.getNickname() + "!");

            }
        else
            System.out.println("Oh, I don't have a pet!");
    }

    public void makeup() {

        System.out.println("It's time for makeup, my lovely " + getName() + "!");

    }

}
