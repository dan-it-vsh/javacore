package com.homework.hw16.Humans;

import com.homework.hw16.Constants.DataUtil;

public interface HumanCreator {

    Human bornChild(String nameMens, String nameWomens);

}
