package com.homework.hw16.Humans;

import com.homework.hw16.Constants.DataUtil;
import com.homework.hw16.Families.Family;
import com.homework.hw16.Pets.Pet;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Random;

public class Human implements DataUtil {

    private String name;
    private String surname;
    private long birthDate;  // toEpochMilli();
    private int iqLevel;
    private Map<String, String> schedule;
    private Family family;

    static {
        System.out.println("New class "
                + Human.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor for all fields of the class Human
    public Human(String name, String surname, long birthDate,
                 int iqLevel, Map<String, String> schedule,
                 Family family) {

        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
        this.family = family;

    }

    // constructor
    public Human(String name, String surname, String birthDateAsString,
                 int iqLevel) {

        this.name = name;
        this.surname = surname;
        this.iqLevel = iqLevel;
        this.birthDate = birthDateAsStringParser(birthDateAsString);

    }

    private long birthDateAsStringParser(String birthDateAsString) {

        long result;

        try {

            LocalDate date = LocalDate.parse(birthDateAsString, DateTimeFormatter.ofPattern(DATE_FORMAT));
            ZoneOffset zoneOffset = date.atStartOfDay(ZoneId.of(TIME_ZONE)).getOffset();
            result = date.atStartOfDay().toInstant(zoneOffset).toEpochMilli();

        } catch (DateTimeParseException e) {

            result = Long.MIN_VALUE;

        }

        return result;
    }

    // START getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iqLevel;
    }

    public void setIq(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    // END getters and setters

    public String describeAge() {

        LocalDate dateNow = LocalDate.now(ZoneId.of(TIME_ZONE));                       //Today's instance
        Instant birthdayInstance = Instant.ofEpochMilli(birthDate);     //Birth date instance

        Period period = Period.between(
                birthdayInstance.atZone(ZoneId.of(TIME_ZONE)).toLocalDate(),
                dateNow
        );

        int days = period.getDays();
        int months = period.getMonths();
        int years = period.getYears();

        String result = "Age: " + years + " years " + months + " months " + days + " days.";

        return result;

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human myHuman = (Human) obj;
        return (name.equals(myHuman.getName())
                && surname.equals(myHuman.getSurname())
                && birthDate == myHuman.getBirthDate()
        );
    }

    @Override
    public int hashCode() {
        int result = 11;
        int coef = 31;
        result = coef * result + name.hashCode();
        result = coef * result + surname.hashCode();
        result = coef * result + (int) birthDate;
        return result;
    }

    public void greetPets() {
        if (family != null && family.getPets() != null) {

            for (Pet myPet : family.getPets()) {

                System.out.println("Hello, " + myPet.getNickname() + "!");

            }
        } else
            System.out.println("Oh, I don't have a pet!");
    }

    public void describePets() {
        if (family != null && family.getPets() != null) {

            for (Pet myPet : family.getPets()) {

                String isTricky = "";
                if (myPet.getTrickLevel() <= 50) isTricky = "not ";
                System.out.println("I have a " + myPet.getSpecies() + ", it's " + myPet.getAge() +
                        " years old, it's " + isTricky + "very cunning!");

            }

        } else

            System.out.println("Oh, I don't have a pet! Nothing to describe!");

    }

    public boolean feedPets(boolean isTimeToFeed) {
        if (family.getPets() == null) return false;

        boolean feedingAtLeastOnce = false;

        if (isTimeToFeed) {

            for (Pet myPet : family.getPets()) {

                System.out.println("Feeding " + myPet.getNickname() + "!");

            }

            return true;

        } else {

            for (Pet myPet : family.getPets()) {

                Random random = new Random();

                if (myPet.getTrickLevel() > random.nextInt(101)) {

                    System.out.println("Let's feed " + myPet.getNickname() + "!");
                    feedingAtLeastOnce = true;

                } else {

                    System.out.println("I think " + myPet.getNickname() + " is not hungry!");

                }

            }
            return feedingAtLeastOnce;
        }
    }

    public String toString() {
        return "" + getClass().getSimpleName() + "{name=\'" + name + "\', surname=\'" + surname +
                "\', birthdate=\'" +
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.of(TIME_ZONE)).toLocalDateTime().format(DateTimeFormatter.ofPattern(DATE_FORMAT)) +
                "\', iq=" + iqLevel +
                ", schedule=" + schedule + "}";
    }

    public String prettyFormat() {
        return "{name=\'" + name + "\', surname=\'" + surname +
                "\', birthdate=\'" +
                Instant.ofEpochMilli(birthDate).atZone(ZoneId.of(TIME_ZONE)).toLocalDateTime().format(DateTimeFormatter.ofPattern(DATE_FORMAT)) +
                "\' iq=" + iqLevel +
                ", schedule=" + schedule + "}";

    }

    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}
