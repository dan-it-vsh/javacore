package com.homework.hw16.Constants;

public enum Species {

    DOG(false, 4, true),
    FISH(false, 0, false),
    DOMESTICCAT(false, 4, true),
    ROBOCAT(false, 4, false),
    UNKNOWN(false,0,false);

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {

        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;

    }

    private final boolean canFly;
    private final int numberOfLegs;
    private boolean hasFur;

    public boolean getCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getHasFur() {
        return hasFur;
    }

}
