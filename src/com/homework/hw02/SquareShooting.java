package com.homework.hw02;

import java.util.Scanner;
import java.util.Random;

public class SquareShooting {
    public static void main(String[] args) {
        // define the fighting field size
        int rowMaxNumber = 5;
        int colMaxNumber = 5;
        // +1 is for agenda 1st row and 1st column
        char[][] fightingFieldArray = new char[rowMaxNumber + 1][colMaxNumber + 1];

        fillFightingSquareOut(fightingFieldArray);
        printFightingSquare(fightingFieldArray);

        // set a Target
        int[] target = setTarget(rowMaxNumber, colMaxNumber);
//        System.out.println("X = " + target[0] + " , Y = " + target[1]);

        // let's start message
        System.out.println("All set. Get ready to rumble!");

        while (true) {
            // entering X and Y for the target
            int targetX = enteringTargetCoordinate(rowMaxNumber, "row (horizontal");
            int targetY = enteringTargetCoordinate(colMaxNumber, "column (vertical");

            if (targetX == target[0] && targetY == target[1]) {
                fightingFieldArray[targetX][targetY] = 'x';
                System.out.println("You have won!");
                printFightingSquare(fightingFieldArray);
                break;
            }

            else
                fightingFieldArray[targetX][targetY] = '*';

            printFightingSquare(fightingFieldArray);
        }
    }

    private static void fillFightingSquareOut(char[][] fieldArray) {
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[0].length; j++)
                if (i == 0) fieldArray[i][j] = Character.forDigit(j, 10);
                else if (j == 0) fieldArray[i][j] = Character.forDigit(i, 10);
                else fieldArray[i][j] = '-';
        }
    }

    private static void printFightingSquare(char[][] fieldArray) {
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[0].length; j++)
                System.out.print(Character.toString(fieldArray[i][j]) + " | ");
            System.out.println();
        }
    }

    private static int[] setTarget(int xMaxIndex, int yMaxIndex) {
        // generating a random value X and Y
        Random random = new Random();
        int xTarget = random.nextInt(xMaxIndex) + 1;
        int yTarget = random.nextInt(yMaxIndex) + 1;
        return new int[]{xTarget, yTarget};
    }

    private static int enteringTargetCoordinate(int maxNumber, String coordinateDescr) {
        int inputNumber;
        System.out.println("Entering the " + coordinateDescr + " line) of the shooting ...");

        while (true) {
            Scanner input = new Scanner(System.in);

            System.out.print("Please enter a number between 1 and " + maxNumber + " : ");
            String inputString = input.nextLine();

            if (isInteger(inputString)) {
                inputNumber = Integer.parseInt(inputString);
            } else {
                System.out.println("Your input is not a correct number!");
                continue;
            }

            if ( inputNumber >= 1 && inputNumber <= maxNumber )
                return inputNumber;
            else
                System.out.println("Your number must be between 1 and " + maxNumber + "!");
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false - is Integer
        return true;
    }

}
