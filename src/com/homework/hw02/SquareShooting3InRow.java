package com.homework.hw02;

import java.util.Random;
import java.util.Scanner;

public class SquareShooting3InRow {
    public static void main(String[] args) {
        // define the fighting field size
        int rowMaxNumber = 5;
        int colMaxNumber = 5;
        // +1 is for agenda 1st row and 1st column
        char[][] fightingFieldArray = new char[rowMaxNumber + 1][colMaxNumber + 1];

        fillFightingSquareOut(fightingFieldArray);
        printFightingSquare(fightingFieldArray);

        // set a Target
        int[][] target = setTarget3InRow(rowMaxNumber, colMaxNumber);

//        System.out.println("---     Target    ---");
//        for (int i = 0; i < target.length; i++) {
//            for (int j = 0; j < target[0].length; j++)
//                System.out.print(target[i][j] + " | ");
//            System.out.println("");
//        }
//        System.out.println("--- End of target ---");

        // let's start message
        System.out.println("All set. Get ready to rumble!");

        int onTarget = 0;
        while (true) {
            // entering X and Y for the target
            int targetX = enteringTargetCoordinate(rowMaxNumber, "row (horizontal");
            int targetY = enteringTargetCoordinate(colMaxNumber, "column (vertical");

            if (isOnTarget(target,targetX,targetY)) {
                fightingFieldArray[targetX][targetY] = 'x';
                if (onTarget++ >=2) {
                    System.out.println("You have won!");
                    printFightingSquare(fightingFieldArray);
                    System.out.println("And you are the WINNER!");
                    break;
                }
            } else
                fightingFieldArray[targetX][targetY] = '*';

            printFightingSquare(fightingFieldArray);
        }
    }

    private static void fillFightingSquareOut(char[][] fieldArray) {
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[0].length; j++)
                if (i == 0) fieldArray[i][j] = Character.forDigit(j, 10);
                else if (j == 0) fieldArray[i][j] = Character.forDigit(i, 10);
                else fieldArray[i][j] = '-';
        }
    }

    private static void printFightingSquare(char[][] fieldArray) {
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[0].length; j++)
                System.out.print(Character.toString(fieldArray[i][j]) + " | ");
            System.out.println();
        }
    }

    private static int[][] setTarget3InRow(int xMaxIndex, int yMaxIndex) {
        // define array to keep 3 pairs(x,y) for the target
        int[][] target = new int[3][2];

        // generating a random value X and Y
        Random random = new Random();

        // randomly generating 1st part of the target
        // 1st pair (x,y)
        target[0][0] = random.nextInt(xMaxIndex) + 1;
        target[0][1] = random.nextInt(yMaxIndex) + 1;

        // generating 2nd part of the target
        // defining where we can expand out 1st part of the target
        // aux targetTmp[targetTmpIndex] with possibile 2nd coordinates
        int[][] targetTmp = new int[4][2];
        int targetTmpIndex = 0;
        // move to the left expanding the target
        targetTmp[targetTmpIndex][0] = target[0][0];
        targetTmp[targetTmpIndex++][1] = target[0][1] - 1;
        // move to the right expanding the target
        targetTmp[targetTmpIndex][0] = target[0][0];
        targetTmp[targetTmpIndex++][1] = target[0][1] + 1;
        // check if we can move to the up expanding the target
        targetTmp[targetTmpIndex][0] = target[0][0] - 1;
        targetTmp[targetTmpIndex++][1] = target[0][1];
        // move to the bottom expanding the target
        targetTmp[targetTmpIndex][0] = target[0][0] + 1;
        targetTmp[targetTmpIndex][1] = target[0][1];

        // exclude points that are outside of the fighting area
        targetTmp = checkTmpTarget(targetTmp, xMaxIndex, yMaxIndex);

        // choose randomly the expanding direction to get 2nd part pf teh target
        int randomIndex = random.nextInt(targetTmp.length);

        target[1][0] = targetTmp[randomIndex][0];
        target[1][1] = targetTmp[randomIndex][1];

        // generating 3rd part of the target
        targetTmpIndex = 0;
        targetTmp = new int[2][2];
        // if 2 first parts are horizontal
        if (target[0][0] == target[1][0]) {
            targetTmp[targetTmpIndex][0] = target[0][0];
            if (target[0][1] < target[1][1]) {
                targetTmp[targetTmpIndex++][1] = target[0][1] - 1;
                targetTmp[targetTmpIndex][1] = target[1][1] + 1;
            } else {
                targetTmp[targetTmpIndex++][1] = target[1][1] - 1;
                targetTmp[targetTmpIndex][1] = target[0][1] + 1;
            }
            targetTmp[targetTmpIndex][0] = target[0][0];
        }

        // if 2 first parts are vertical
        else {
            targetTmp[targetTmpIndex][1] = target[0][1];
            if (target[0][0] < target[1][0]) {
                targetTmp[targetTmpIndex++][0] = target[0][0] - 1;
                targetTmp[targetTmpIndex][0] = target[1][0] + 1;
            } else {
                targetTmp[targetTmpIndex++][0] = target[1][0] - 1;
                targetTmp[targetTmpIndex][0] = target[0][0] + 1;
            }
            targetTmp[targetTmpIndex][1] = target[0][1];
        }

        // exclude points that are outside of the fighting area
        targetTmp = checkTmpTarget(targetTmp, xMaxIndex, yMaxIndex);

        // choose randomly the expanding direction to get 3rd part pf the target
        randomIndex = random.nextInt(targetTmp.length);

        target[2][0] = targetTmp[randomIndex][0];
        target[2][1] = targetTmp[randomIndex][1];

        return target;
    }

    private static int enteringTargetCoordinate(int maxNumber, String coordinateDescr) {
        int inputNumber;
        System.out.println("Entering the " + coordinateDescr + " line) of the shooting ...");

        while (true) {
            Scanner input = new Scanner(System.in);

            System.out.print("Please enter a number between 1 and " + maxNumber + " : ");
            String inputString = input.nextLine();

            if (isInteger(inputString)) {
                inputNumber = Integer.parseInt(inputString);
            } else {
                System.out.println("Your input is not a correct number!");
                continue;
            }

            if (inputNumber >= 1 && inputNumber <= maxNumber)
                return inputNumber;
            else
                System.out.println("Your number must be between 1 and " + maxNumber + "!");
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false - is Integer
        return true;
    }

    private static int[][] checkTmpTarget(int[][] targetArray, int xMaxIndex, int yMaxIndex) {
        // we need to throw away elements that are outside the fighting field
        int targetNewIndex = 0;
        int[][] newTargetArray = new int[1][2];

        // check if we are within the fighting area and if we are, keep the pair
        for (int i = 0; i < targetArray.length; i++)
            if (targetArray[i][0] > 0 && targetArray[i][0] <= xMaxIndex &&
                    targetArray[i][1] > 0 && targetArray[i][1] <= yMaxIndex) {
                try {
                    newTargetArray[targetNewIndex][0] = targetArray[i][0];
                    newTargetArray[targetNewIndex][1] = targetArray[i][1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    newTargetArray = newArrayAllocate(newTargetArray);
                    newTargetArray[targetNewIndex][0] = targetArray[i][0];
                    newTargetArray[targetNewIndex][1] = targetArray[i][1];
                }
                targetNewIndex++;
            }
        return newTargetArray;
    }

    private static int[][] newArrayAllocate(int[][] arr) {
        // increasing array length by 1 row
        int[][] newArr = new int[arr.length + 1][arr[0].length];

        // copying existing values to the new array
        for (int i = 0; i < arr.length; i++) {
            newArr[i][0] = arr[i][0];
            newArr[i][1] = arr[i][1];
        }
        return newArr;
    }

    private static boolean isOnTarget(int[][] target, int targetX, int targetY) {
        // checking if (targetX, targetY) is in the array target[][2]
        for (int i = 0; i < target.length; i++)
                if (target[i][0] == targetX && target[i][1] == targetY)
                    return true;
        return false;
    }
}
