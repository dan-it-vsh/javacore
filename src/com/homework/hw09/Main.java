package com.homework.hw09;

public class Main {
    public static void main(String[] args) {
        int objectsNumber1 = 1000000;

//        for (int i = 0; i < objectsNumber1; i++)
//            new Human("Vasia","Pupkin", 1945);

        String[][] schedule = new String[7][2];

        schedule[0][0] = DaysOfWeek.SUNDAY.name();
        schedule[0][1] = "do homework in java core.";
        schedule[1][0] = DaysOfWeek.MONDAY.name();
        schedule[1][1] = "work; go to courses.";
        schedule[2][0] = DaysOfWeek.TUESDAY.name();
        schedule[2][1] = "work; do homework in java core.";
        schedule[3][0] = DaysOfWeek.WEDNESDAY.name();
        schedule[3][1] = "work; go to courses; watch a Champions league match.";
        schedule[4][0] = DaysOfWeek.THURSDAY.name();
        schedule[4][1] = "work; do java homework; watch a Europa league match.";
        schedule[5][0] = DaysOfWeek.FRIDAY.name();
        schedule[5][1] = "work; do java homework; preparation to java classes.";
        schedule[6][0] = DaysOfWeek.SATURDAY.name();
        schedule[6][1] = "go to major Java course.";

        Human boy = new Human("Porfirii", "Petrov", 2009);
        System.out.println(boy.toString());

        boy.setSchedule(schedule);
        System.out.println(boy.toString());

        Pet myPet1 = new Pet(Species.DOG,"vaska", 5);
        System.out.println(myPet1.toString());

        Pet myPet2 = new Pet(Species.FISH,"tom", 2);
        System.out.println(myPet2.toString());

    }
}
