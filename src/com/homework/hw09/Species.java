package com.homework.hw09;

public enum Species {

    DOG(false, 4, true),
    CAT(false, 4, true),
    FISH(false, 0, false),
    MOUSE(false, 4, true);

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {

        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;

    }

    private final boolean canFly;
    private final int numberOfLegs;
    private boolean hasFur;

    public boolean canFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }

}
