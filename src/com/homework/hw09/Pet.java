package com.homework.hw09;

import java.util.Arrays;

public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("New class "
                + Pet.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor for species, nickname and age fields of the class Pet only
    public Pet(Species species, String nickname, int age) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
    }

    // constructor for all fields of the class Pet
    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname, age);
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet myPet = (Pet) obj;
        return (species.equals(myPet.getSpecies())
                && nickname.equals(myPet.getNickname())
                && age == myPet.getAge()
        );
    }

    @Override
    public int hashCode() {
        int result = 13;
        int coef = 37;
        result = coef * result + age;
        result = coef * result + species.hashCode();
        result = coef * result + nickname.hashCode();
        return result;
    }

    // START getters and setters

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    // END getters and setters

    public void eat() {
        System.out.println("I'm eating...");
    }

    public void respond() {
        System.out.println("Hello, boss!. I am - " + this.nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("It's needed to cover up my tracks...");
    }

    public String toString() {
        return species + "{nickname=\'" + nickname + "\', age=" + age +
                "," + " trickLevel=" + trickLevel + "," +
                " \'can fly\'=" + species.canFly() + "," +
                " \'has fur\'=" + species.hasFur() + "," +
                " \'number of legs\'=" + species.getNumberOfLegs() + "," +
                " habits=" + Arrays.toString(habits) + "}";
    }

    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}

