package com.homework.hw04;

public class HappyFamily {
    public static void main(String[] args) {

        // mother
        Human mother = new Human("Khrystia", "Petrova", 1984, 101);
        System.out.println(mother.toString());

        // father
        Human father = new Human("Vasia", "Petrov", 1981, 102);
        System.out.println(father.toString());

        // boy
        Human boy = new Human("Petia", "Petrov", 2005, 103);
        System.out.println(boy.toString());

        // adding mother and father to boy
        boy.addFather(father);
        boy.addMother(mother);

        // defining a pet
        String[] habits = {"playing", "sleeping", "hanging"};
        Pet myPet1 = new Pet("cat", "Zigmund", 7, 55, habits);

        // adding a pet to boy
        boy.addPet(myPet1);

        System.out.println(boy.toString());

        // Pet's methods
        myPet1.eat();
        myPet1.respond();
        myPet1.foul();
        System.out.println(myPet1.toString());

        // Human's methods
        boy.greetPet();
        boy.describePet();
        System.out.println("Trick level is = " + myPet1.getTrickLevel() + ".");
        myPet1.setTrickLevel(20);
        boy.describePet();
        System.out.println("Trick level is = " + myPet1.getTrickLevel() + ".");

        myPet1.setTrickLevel(5);
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(true) + "!");
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(false) + "!");
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(false) + "!");
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(false) + "!");

        myPet1.setTrickLevel(95);
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(false) + "!");
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(false) + "!");
        System.out.println("Is " + myPet1.getNickname() + " being fed? : " + boy.feedPet(true) + "!");
    }
}
