package com.homework.hw04;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("I'm eating...");
    }

    public void respond() {
        System.out.println("Hello, boss!. I am - " + this.nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("It's needed to cover up my tracks...");
    }

    public String toString() {
        return this.species+"{nickname=\'" + this.nickname + "\', age=" + this.age +
                "," + " trickLevel=" + this.trickLevel + "," +
                " habits=" + Arrays.toString(this.habits) + "}";
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getSpecies() {
        return this.species;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
}
