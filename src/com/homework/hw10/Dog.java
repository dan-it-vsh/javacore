package com.homework.hw10;

public class Dog extends Pet implements MakingAFoul {

    static {

        System.out.println("New class "
                + Dog.class.getSimpleName()
                + " is loading...");
    }

    {

        System.out.println("New object " + getClass().getSimpleName() + " is creating...");

        setSpecies(Species.DOG);

    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {

        super(nickname, age, trickLevel, habits);

    }

    public void respond() {
        System.out.println("Hello, boss!. I am - " + getNickname() + ". I miss you!");
    }

    @Override
    public void foul() {

        System.out.println("I am hiding my bone deep...");

    }

}
