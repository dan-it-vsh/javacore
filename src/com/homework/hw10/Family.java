package com.homework.hw10;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;

public class Family implements HumanCreator {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    static {
        System.out.println("New class "
                + Family.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family myFamily = (Family) obj;
        return (mother.equals(myFamily.getMother())
                && father.equals(myFamily.getFather())
        );
    }

    @Override
    public int hashCode() {
        int result = 17;
        int coef = 31;
        result = coef * result + mother.hashCode();
        result = coef * result + father.hashCode();
        return result;
    }

    public Human getMother() {
        return mother;
    }

//    public void setMother(Human mother) {
//        this.mother = mother;
//    }

    public Human getFather() {
        return father;
    }

//    public void setFather(Human father) {
//        this.father = father;
//    }

    public Human[] getChildren() {
        return children;
    }

    public void addChild(Human child) {
//        if (childIsInChildren(child))
//            throw new Error("You're trying to add already existing-in-family child : " + child.toString());
        if (!childIsInChildren(child)) {
            if (child != null) {
                child.setFamily(this);

                Human[] newChildren = new Human[children.length + 1];

                for (int i = 0; i < children.length; i++) {
                    newChildren[i] = children[i];
                }

                newChildren[newChildren.length - 1] = child;
                children = newChildren;
            }
        }
    }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            for (int i = 0, j = 0; i < children.length; i++) {
                if (i != index)
                    newChildren[j++] = children[i];
                else children[i].setFamily(null);
            }
            children = newChildren;
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child) {
//        if (!childIsInChildren(child)) throw new Error("You're trying to delete un-existing-in-family child : " + child.toString());
        if (childIsInChildren(child)) {
            Human[] newChildren = new Human[children.length - 1];
            for (int i = 0, j = 0; i < children.length; i++) {
                if (!children[i].equals(child))
                    newChildren[j++] = children[i];
                else children[i].setFamily(null);
            }
            children = newChildren;
            return true;
        } else
            return false;
    }

    private boolean childIsInChildren(Human child) {
        if (child == null) return false;
        for (int i = 0; i < children.length; i++)
            if (children[i].hashCode() == child.hashCode())
                if (children[i].equals(child)) return true;
        return false;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public int countFamily() {
        return 2 + children.length;
    }

    @Override
    public Human bornChild() {

        if (father != null && mother != null) {

            Human child;

            // initializing child's parameters
            String name;

            String surname;
            if (father.getSurname() != null) {
                surname = father.getSurname();
            } else surname = "Unknown";

            int year = Calendar.getInstance().get(Calendar.YEAR);

            int iq = (int) ((father.getIq() + mother.getIq()) / 2);

            String[][] schedule = new String[][]{};

            Family childFamily = father.getFamily();

            // Woman: 0; Man: 1
            int sex = 0;

            Random random = new Random();
            if ( random.nextInt(101) > 50) sex = 1;

            if (sex == 1) {

                name = NamesMens.values()[random.nextInt(NamesMens.values().length)].getName();
                child = new Man(name, surname, year, iq, schedule, childFamily);

            } else {

                surname = surname + "a";
                name = NamesWomens.values()[random.nextInt(NamesWomens.values().length)].getName();
                child = new Woman(name, surname, year, iq, schedule, childFamily);

            }

            addChild(child);// if child exists in Chidren, it is doing nothing
            return child;

        }
        return null;
    }

    public String toString() {
        String motherFullName = null, fatherFullName = null;

        if (mother != null)
            motherFullName = mother.getName() + " " + mother.getSurname();

        if (father != null)
            fatherFullName = father.getName() + " " + father.getSurname();

        return "Family{mother=\'" + motherFullName + "\'" +
                " father=\'" + fatherFullName + "\'" +
                " children=\'" + Arrays.toString(getChildren()) + "\'" +
                " pet=\'" + pet + "\'}";
    }

    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}
