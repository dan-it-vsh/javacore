package com.homework.hw10;

import java.util.Arrays;

abstract class Pet {

    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("New class "
                + Pet.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + getClass().getSimpleName() + " is creating...");
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet myPet = (Pet) obj;
        return (species.equals(myPet.getSpecies())
                && nickname.equals(myPet.getNickname())
                && age == myPet.getAge()
        );
    }

    @Override
    public int hashCode() {
        int result = 13;
        int coef = 37;
        result = coef * result + age;
        result = coef * result + species.hashCode();
        result = coef * result + nickname.hashCode();
        return result;
    }

    // START getters and setters

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    // END getters and setters

    public void eat() {
        System.out.println("I'm eating...");
    }

    abstract void respond();

    public String toString() {
        return species + "{nickname=\'" + nickname + "\', age=" + age +
                "," + " trickLevel=" + trickLevel + "," +
                " \'can fly\'=" + species.getCanFly() + "," +
                " \'has fur\'=" + species.getHasFur() + "," +
                " \'number of legs\'=" + species.getNumberOfLegs() + "," +
                " habits=" + Arrays.toString(habits) + "}";
    }

    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}

