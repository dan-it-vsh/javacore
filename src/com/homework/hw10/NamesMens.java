package com.homework.hw10;

public enum NamesMens {

    VASILII("Vasilii"),
    PETR("Petr"),
    ALEKSANDR("Aleksamdr"),
    VLADIMIR("Vladimir"),
    IGOR("Igor"),
    PORFILII("Porfirii"),
    ANTON("Anton"),
    SAVELII("Savelii"),
    NIKOLAI("Nikolai"),
    DENIS("Denis");

    private final String name;

    NamesMens(String nameMens) {
        name = nameMens;
    }

    public String getName() {
        return name;
    }

}
