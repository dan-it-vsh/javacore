package com.homework.hw10;

public final class Woman extends Human {

    static {

        System.out.println("New class "
                + Woman.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");

    }

    Woman(String name, String surname, int yearBirthDay,
                 int iqLevel, String[][] schedule,
                 Family family) {

        super(name, surname, yearBirthDay, iqLevel, schedule, family);

    }

    @Override
    public void greetPet() {
        if (getFamily() != null && getFamily().getPet() != null)
            System.out.println("Hello, my cute " + getFamily().getPet().getNickname() + "!");
        else
            System.out.println("Oh, I don't have a pet!");
    }

    public void makeup() {

            System.out.println("It's time for makeup, my lovely " + getName() + "!");

    }

}
