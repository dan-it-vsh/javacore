package com.homework.hw10;

public class Main {
    public static void main(String[] args) {

        Dog myDog = new Dog("Sharik", 2, 20, new String[]{"eating", "sleepimg"});
        System.out.println(myDog);
        myDog.eat();
        myDog.respond();
        myDog.foul();

        RoboCat myRoboCat = new RoboCat("Murzik", 1, 90, new String[]{"eating", "sleeping", "running"});
        System.out.println(myRoboCat);
        myRoboCat.eat();
        myRoboCat.respond();
        myRoboCat.foul();

        Fish myFish = new Fish("Ponchik", 3, 30, new String[]{"swimming"});
        System.out.println(myFish);
        myFish.eat();
        myFish.respond();

        DomesticCat myDomCat = new DomesticCat("Vaska", 3, 30, new String[]{"lazy", "eating"});
        System.out.println(myDomCat);
        myDomCat.eat();
        myDomCat.respond();
        myDomCat.foul();

        Man human1 = new Man("Petr", "Ivanov", 1980, 100, null, null);
        Woman human2 = new Woman("Fekla", "Petrova", 1985, 110, null, null);

        human1.greetPet();
        human1.repairCar();
        human2.greetPet();
        human2.makeup();

        Family myFamily1 = new Family(human2, human1);
        myFamily1.setPet(myDomCat);
        human1.greetPet();
        human2.greetPet();

        System.out.println(myFamily1);
        System.out.println(human1);
        System.out.println(human2);

        for (int i = 0; i < 5; i++) {

            Human child = myFamily1.bornChild();
            System.out.println(child);

            System.out.println(child.getFamily());
            System.out.println("Number of family members = " + child.getFamily().countFamily());

            child.greetPet();

        }


    }


}
