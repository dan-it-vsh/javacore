package com.homework.hw10;

public class Fish extends Pet {

    static {

        System.out.println("New class "
                + Fish.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + getClass().getSimpleName() + " is creating...");

        setSpecies(Species.FISH);

    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {

        super(nickname, age, trickLevel, habits);

    }

    public void respond() {

        System.out.println("O-o-o-o-o. O-o-o-o - " + getNickname() + "!");

    }

}
