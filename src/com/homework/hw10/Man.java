package com.homework.hw10;

public final class Man extends Human {

    static {

        System.out.println("New class "
                + Man.class.getSimpleName()
                + " is loading...");

    }

    {

        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");

    }

    public Man(String name, String surname, int yearBirthDay,
         int iqLevel, String[][] schedule,
         Family family) {

        super(name, surname, yearBirthDay, iqLevel, schedule, family);

    }

    @Override
    public void greetPet() {
        if (getFamily() != null && getFamily().getPet() != null)
            System.out.println("Go walking, " + getFamily().getPet().getNickname() + "!");
        else
            System.out.println("Damn, I don't have a pet!");
    }

    public void repairCar() {

        System.out.println("Let's go, " + getName() + ", to meet the car!");

    }

}
