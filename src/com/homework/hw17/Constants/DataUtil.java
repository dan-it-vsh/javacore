package com.homework.hw17.Constants;

public interface DataUtil {

    final String DATE_FORMAT = "dd/MM/yyyy";
    final String TIME_ZONE = "Europe/Kiev";

}
