package com.homework.hw17.Families;

import com.homework.hw17.Constants.DataUtil;
import com.homework.hw17.Humans.*;
import com.homework.hw17.Constants.NamesMens;
import com.homework.hw17.Constants.NamesWomens;
import com.homework.hw17.Pets.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

public class Family implements HumanCreator, DataUtil {

    private Human father;
    private Human mother;
    private List<Human> children;
    private Set<Pet> pets;

    static {
        System.out.println("New class "
                + Family.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    // constructor
    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        this.children = new ArrayList();
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pets = new HashSet();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family myFamily = (Family) obj;
        return (father.equals(myFamily.getFather())
                && mother.equals(myFamily.getMother())
        );
    }

    @Override
    public int hashCode() {
        int result = 17;
        int coef = 31;
        result = coef * result + father.hashCode();
        result = coef * result + mother.hashCode();
        return result;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void addChild(Human child) {

        if (!children.contains(child)) {
            if (child != null) {

                child.setFamily(this);
                children.add(child);

            }
        }
    }

    public boolean deleteChild(int index) {

        if (index >= 0 && index < children.size()) {

            if (!children.contains(children.get(index))) return false;

            children.get(index).setFamily(null);
            children.remove(children.get(index));
            return true;

        }

        return false;
    }

    public boolean deleteChild(Human child) {

        if (!children.contains(child)) return false;

        child.setFamily(null);
        children.remove(child);
        return true;

    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    public int countFamilyMembers() {
        return 2 + children.size();
    }

    @Override
    public Human bornChild(String nameMens, String nameWomens) {

        Random random = new Random();

        if (father != null && mother != null) {

            Human child;

            // initializing child's parameters
            String name;

            String surname;
            if (father.getSurname() != null) {
                surname = father.getSurname();
            } else surname = "Unknown";

            // randomizing birthDate between the youngest parent + 17 years AND now
            LocalDate date = LocalDate.now();
            ZoneOffset zoneOffset = date.atStartOfDay(ZoneId.of(TIME_ZONE)).getOffset();
            long birthDateRangeEnd = date.atStartOfDay().toInstant(zoneOffset).toEpochMilli();
            long birthDateRangeStart = Instant.ofEpochMilli(Math.max(father.getBirthDate(), mother.getBirthDate())).atZone(ZoneId.of(TIME_ZONE)).plusYears(17L).toInstant().toEpochMilli();
            // random birthDate
            long birthDate = birthDateRangeStart+((long)(random.nextDouble()*(birthDateRangeEnd-birthDateRangeStart)));

            int iq = (int) ((father.getIq() + mother.getIq()) / 2);

            Map<String, String> schedule = new HashMap();

            Family childFamily = father.getFamily();

            // Woman: 0; Man: 1
            int sex = 0;

            if (random.nextInt(101) > 50) sex = 1;

            if (sex == 1) {

                if (nameMens.length() > 0) {

                    name = nameMens;

                } else {

                    name = NamesMens.values()[random.nextInt(NamesMens.values().length)].getName();

                }

                child = new Man(name, surname, birthDate, iq, schedule, childFamily);

            } else {

                if (nameWomens.length() > 0) {

                    name = nameWomens;

                } else {

                    name = NamesWomens.values()[random.nextInt(NamesWomens.values().length)].getName();

                }

//                surname = surname + "a";
                child = new Woman(name, surname, birthDate, iq, schedule, childFamily);

            }

            addChild(child);// if child exists in Chidren, it is doing nothing
            return child;

        }
        return null;
    }

    public String toString() {
        String motherFullName = null, fatherFullName = null;

        if (father != null)
            fatherFullName = father.getName() + " " + father.getSurname();

        if (mother != null)
            motherFullName = mother.getName() + " " + mother.getSurname();

        return "Family{father=\'" + fatherFullName + "\'" +
                " mother=\'" + motherFullName + "\'" +
                " children=\'" + children + "\'" +
                " pets=\'" + pets + "\'}";
    }

    public String prettyFormat() {

        final StringBuilder childrenBuilder = new StringBuilder();

        children.forEach((item) -> {
            String sex;
            if (item.getClass().getSimpleName().equals("Man"))
                sex = "boy";
            else if (item.getClass().getSimpleName().equals("Woman"))
                sex = "girl";
            else sex = "human";
            childrenBuilder.append(sex + ": "
                    + item.prettyFormat() + "\n\t\t\t");
        });

        final StringBuilder petsBuilder = new StringBuilder();
        pets.forEach(item -> petsBuilder.append(item.prettyFormat() + ", "));

        return "family:\n" +
                "\t mother: " + mother.prettyFormat() +
                "\n\t father: " + father.prettyFormat() +
                "\n\t children: \n\t\t\t" + childrenBuilder.toString().trim().replaceAll(",$", "") +
                "\n\t pets: " + petsBuilder.toString().trim().replaceAll(",$", "");

    }


    protected void finalize() {
        System.out.println("Deleting object " + toString());
    }

}
