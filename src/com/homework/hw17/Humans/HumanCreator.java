package com.homework.hw17.Humans;

import com.homework.hw17.Constants.DataUtil;

public interface HumanCreator {

    Human bornChild(String nameMens, String nameWomens);

}
