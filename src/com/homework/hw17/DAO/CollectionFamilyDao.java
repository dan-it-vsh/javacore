package com.homework.hw17.DAO;

import com.homework.hw17.Families.Family;
import com.homework.hw17.Humans.Human;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familiesList;

    static {
        System.out.println("New class "
                + CollectionFamilyDao.class.getSimpleName()
                + " is loading...");
    }

    {
        System.out.println("New object " + this.getClass().getSimpleName() + " is creating...");
    }

    public CollectionFamilyDao() {
        familiesList = new ArrayList();
    }

    @Override
    public List<Family> getAllFamilies() {
        return familiesList;
    }

    @Override
    public Family getFamilyByIndex(int index) {

        Family result = null;

        if (index >= 0 && index < familiesList.size())
            result = familiesList.get(index);

        return result;
    }

    @Override
    public boolean deleteFamily(int index) {

        boolean result = false;

        if (index >= 0 && index < familiesList.size()) {
            familiesList.remove(index);
            result = true;
        }

        return result;

    }

    @Override
    public boolean deleteFamily(Family family) {

        return familiesList.remove(family);

    }

    @Override
    public void saveFamily(Family family) {

        if (family != null) {

            if (familiesList.contains(family)) {

                familiesList.set(familiesList.indexOf(family),family);

            } else {

                familiesList.add(family);

            }
        }

    }

    @Override
    public Family createFamily(Human father, Human mother) {

        return new Family(father, mother);

    }


    public int numberOfFamilies() {

        return familiesList.size();

    }

}
