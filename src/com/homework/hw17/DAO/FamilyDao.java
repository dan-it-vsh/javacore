package com.homework.hw17.DAO;

import com.homework.hw17.Families.Family;
import com.homework.hw17.Humans.Human;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int Index);

    boolean deleteFamily(int Index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    Family createFamily(Human father, Human mother);

}
