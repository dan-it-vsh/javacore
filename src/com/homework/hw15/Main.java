package com.homework.hw15;

import com.homework.hw15.Families.FamilyController;
import com.homework.hw15.Humans.Human;

public class Main {

    public static void main(String[] args) {

        FamilyController controller = new FamilyController();

        System.out.println(controller.getAllFamilies());

        controller.displayAllFamilies();

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", "0e7/07/1980", 110),
                new Human("Paraska", "Pupkina", "10/12/1982", 105)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", "07/07/1980", 110),
                new Human("Paraska", "Pupkina", "10/12/1982", 105)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", "07/08/1981", 110),
                new Human("Paraska", "Pupkina", "11/11/1983", 105)
        );

        controller.createNewFamily(
                new Human("Vasia", "Pupkin", "07/08/1981", 110),
                new Human("Fekla", "Pupkina", "11/11/1983", 105)
        );

        controller.displayAllFamilies();

        controller.getFamilyById(0).bornChild("Petia", "Masha");
        controller.adoptChild(controller.getFamilyById(0), new Human("Denis", "Sidorov", "11/05/2002", 110));

        controller.displayAllFamilies();

        System.out.println(controller.getFamilyById(0).getFather());
        System.out.println(controller.getFamilyById(0).getFather().describeAge());
        System.out.println(controller.getFamilyById(0).getMother());
        System.out.println(controller.getFamilyById(0).getMother().describeAge());

        System.out.println(controller.getFamilyById(1).getFather());
        System.out.println(controller.getFamilyById(1).getFather().describeAge());
        System.out.println(controller.getFamilyById(1).getMother());
        System.out.println(controller.getFamilyById(1).getMother().describeAge());

        System.out.println(controller.getFamilyById(2).getFather());
        System.out.println(controller.getFamilyById(2).getFather().describeAge());
        System.out.println(controller.getFamilyById(2).getMother());
        System.out.println(controller.getFamilyById(2).getMother().describeAge());

        controller.adoptChild(controller.getFamilyById(0),
                new Human("Vasia1", "Pupkin1", "24/12/2005", 101));

        controller.adoptChild(controller.getFamilyById(0),
                new Human("Vasia2", "Pupkin2", "23/12/2005", 102));

        controller.adoptChild(controller.getFamilyById(0),
                new Human("Vasia3", "Pupkin3", "25/12/2005", 103));

        System.out.println(controller.getFamilyById(0));
        System.out.println("Number of children = " + controller.getFamilyById(0).getChildren().size());

        controller.deleteAllChildrenOlderThen(13);
        System.out.println(controller.getFamilyById(0));
        System.out.println("Number of children = " + controller.getFamilyById(0).getChildren().size());


    }


}


