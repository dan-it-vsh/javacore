package com.homework.hw03;

import java.util.Scanner;

public class Scheduler {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework in java core.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "work; go to courses.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "work; do homework in java core.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "work; go to courses; watch a Champions league match.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "work; do java homework; watch a Europa league match.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "work; do java homework; preparation to java classes.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to major Java course.";

        while (true) {
            String day = inputWeekDay();
            switch (day) {
                case "Monday":
                    printTasks("Monday", schedule);
                    break;
                case "Tuesday":
                    printTasks("Tuesday", schedule);
                    break;
                case "Wednesday":
                    printTasks("Wednesday", schedule);
                    break;
                case "Thursday":
                    printTasks("Thursday", schedule);
                    break;
                case "Friday":
                    printTasks("Friday", schedule);
                    break;
                case "Saturday":
                    printTasks("Saturday", schedule);
                    break;
                case "Sunday":
                    printTasks("Sunday", schedule);
                    break;
                case "Exit":
                    return;
                default:
                    if (day.startsWith("Change ") || day.startsWith("Reschedule ")) {

                        String[] dayArray = day.trim().split("\\s+", -1);
                        if (dayArray.length > 1 &&
                                getActivity(dayArray[1], schedule) != -1) {
                            changeTask(dayArray[1], schedule, inputNewTask(dayArray[1]));
                            break;
                        } else {
                            System.out.println("Sorry, the day seems to be wrong, please try again.");
                            break;
                        }
                    }
                    System.out.println("Sorry, I don't understand you, please try again.");

            }
        }
    }

    private static String inputWeekDay() {
        System.out.println("Please, input the day of the week or action: ");
        Scanner input = new Scanner(System.in);
        String inputString = input.nextLine().trim();

        if (!inputString.isEmpty()) {

            String[] inputArray = inputString.trim().split("\\s+", -1);

            if (inputArray.length == 1) {

                inputString = inputString.trim().substring(0, 1).toUpperCase() + inputString.trim().substring(1, inputString.trim().length()).toLowerCase();

            } else if (inputArray.length > 1) {

                inputString = "";
                // We only take 2 first words and throw away the rest
                for (int i = 0; i < 2; i++) {
                    inputString += inputArray[i].trim().substring(0, 1).toUpperCase() + inputArray[i].trim().substring(1, inputArray[i].trim().length()).toLowerCase() + " ";

                }
            }
        }
        return inputString;
    }

    private static int getActivity(String day, String[][] schedule) {
        for (int i = 0; i < schedule.length; i++)
            if (schedule[i][0].equals(day))
                return i;
        return -1;
    }

    private static void printTasks(String day, String[][] schedule) {
        System.out.println("Your tasks for " + day + " are : " + schedule[getActivity(day, schedule)][1]);

    }

    private static String inputNewTask(String day) {
        System.out.println("Please, input new tasks for " + day);
        Scanner input = new Scanner(System.in);

        return input.nextLine().trim();

    }

    private static void changeTask(String day, String[][] schedule, String newTask) {
        schedule[getActivity(day, schedule)][1] = newTask;
    }

}