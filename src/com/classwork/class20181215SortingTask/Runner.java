package com.classwork.class20181215SortingTask;

import com.classwork.lesson20181215TreeStructure.User;

import java.util.*;

public class Runner {
    public static void main(String[] args) {
        List<User> userList = Arrays.asList(
                new User("vasia", "pupkin", 39),
                new User("petia", "vasechkin", 33),
                new User("porfirii", "ivanov", 43)
        );

        System.out.println(userList);

        sort(userList);
//        sort(userList,User::compareTo);
        System.out.println(userList);


//        System.out.println(sort(new Array {0,5,2,3,4,4}));


    }

    static <T extends Comparable<T>> List<T> sort(List<T> list) {

        List<T>  result = new ArrayList();
        Collections.sort(list);

        return list;

    }

    static <T> List<T> sort(List<T> list, Comparator<T> comparator) {

        Collections.sort(list, comparator);
        return list;
    }

    public int[] sort(int[] elems) {
        for (int i = 0; i < elems.length; i++) {
            for (int j = i + 1; j < elems.length; j++) {
                if (elems[i] > elems[j]) {
                    swap(i, j, elems);
                }
            }
        }
        return elems;
    }

    public void swap(int i, int j, int[] elems) {

        int temp = elems[i];
        elems[i] = elems[j];
        elems[j] = temp;

    }

}
