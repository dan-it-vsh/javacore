package com.classwork.class20181208ArrayList;

import com.sun.javafx.collections.ListListenerHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListService {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList();
        list.add(1);
        list.add(2);

        System.out.println(list);

        ArrayList<Integer> list2 = new ArrayList();
        list2.add(3);
        list.addAll(list2);

        System.out.println(list);


        List<String> linkedList = new LinkedList();
        List<String> arrayList = new ArrayList();
        generate(linkedList);
        generate(arrayList);

        long start = System.currentTimeMillis();
//        removeRange(LinkedList);
        System.out.println();



    }

    static void removeRange(List<String> elem) {
        for (int i = 10000; i < 60000; i++) {
            elem.remove(i);
        }
    }

    static void generate(List<String> elems) {
        for (int i = 0; i < 100000; i++){
            elems.add(String.valueOf(i));
        }
    }

}

class Person {
    private List<String> children;

    public Person(List<String> chidren) {
        this.children = chidren;
    }

}