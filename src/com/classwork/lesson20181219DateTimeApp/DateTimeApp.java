package com.classwork.lesson20181219DateTimeApp;

import java.time.*;
import java.util.Date;
import java.util.Locale;

public class DateTimeApp {

    public static void main(String[] args) {

        // java.time -> factory methods
        // Instant

        Date date = new Date();

        Instant now = Instant.now();
        Instant instant = Instant.ofEpochMilli(date.getTime());

        // LocalDate LocalTime
        LocalDate now1 = LocalDate.now();
        LocalTime timeParis = LocalTime.now(ZoneId.of("Europe/Paris"));
        LocalDateTime dayAndTimeInParis = LocalDateTime.of(now1, timeParis);

        System.out.println(now1);
        System.out.println(timeParis);
        System.out.println(dayAndTimeInParis);

        LocalDateTime localDateTime = LocalDateTime.now()
                .withYear(2018)
                .withMonth(10)
                .withDayOfMonth(31);

        System.out.println(localDateTime);


    }

}
