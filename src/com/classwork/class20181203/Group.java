package com.classwork.class20181203;

import java.util.Arrays;

public class Group {
    private String groupDescription;
    private Student[] students;

    public Group(String groupDescription) {
        this.groupDescription = groupDescription;
        this.students = new Student[] {};
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public void addStudent(Student student) {

    }

    @Override
    public String toString() {
        return "Group{" +
                "groupDescription='" + groupDescription + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
