package com.classwork.class20181203;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Subject subj1 = new Subject();
        subj1.subjectInfo();
        subj1.setSubjectName("Math");
        subj1.setSubjectDescription("Mathematics");
        subj1.setTeacher(new Teacher("Vasia", "Pupkin"));
        subj1.subjectInfo();

        Subject subj2 = new Subject("phisycs", "Radio frequency physics");

        System.out.println(subj1.toString());
        System.out.println(subj2.toString());

        Student student1 = new Student("Petia", "Vasechkin");
        Student student2 = new Student("Olga", "Petrova");

        Group group1 = new Group("GroupSIX");
        System.out.println(group1.toString());

        System.out.println(student1.toString());
        System.out.println(student2.toString());


        student1.addMark(new Mark("mathe", 10));
        System.out.println(student1.toString());
        System.out.println(Arrays.toString(student1.getMarks()));


        student1.addMark(new Mark("mathe", 11));
        System.out.println(student1.getMarks());
        System.out.println(Arrays.toString(student1.getMarks()));

        Mark mark1= new Mark("mathe", 11);
        System.out.println(mark1.toString());

        student1.addMark(mark1);
        System.out.println(Arrays.toString(student1.getMarks()));










    }
}
