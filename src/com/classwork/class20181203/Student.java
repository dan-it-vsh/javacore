package com.classwork.class20181203;

import java.util.Arrays;

public class Student {

    private String name;
    private String surname;
    private Mark[] marks;

    Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.marks = new Mark[]{};
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Mark[] getMarks() {
        return marks;
    }

    public void setMarks(Mark[] marks) {
        this.marks = marks;
    }

    public void addMark(Mark mark) {
//        if (!markIsInMarks(mark)) {
//
//

        if (mark != null) {
            Mark[] newMarks = new Mark[marks.length + 1];

            for (int i = 0; i < marks.length; i++) {
                newMarks[i] = marks[i];
            }
            newMarks[marks.length] = mark;
            marks = newMarks;
//            }
//        }
        }
    }


    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", marks=" + Arrays.toString(marks) +
                '}';
    }
}
