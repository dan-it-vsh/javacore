package com.classwork.class20181203;

public class Mark {
    private String subject;
    private int mark;

    Mark(String subject, int mark) {
        this.subject = subject;
        this.mark = mark;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Mark myMark = (Mark) obj;
        return mark == myMark.getMark() &&
                subject.equals(myMark.getSubject());
    }

    @Override
    public int hashCode() {
        int result = 11;
        int coef = 31;
        result = coef * result + mark;
        result = coef * result + subject.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "subject='" + subject + '\'' +
                ", mark=" + mark +
                '}';
    }
}
