package com.classwork.class20181203;

public class Teacher {
    private String tname;
    private String tsurname;

    Teacher(String name, String surname) {
        this.tname = name;
        this.tsurname = surname;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTsurname() {
        return tsurname;
    }

    public void setTsurname(String tsurname) {
        this.tsurname = tsurname;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "tname='" + tname + '\'' +
                ", tsurname='" + tsurname + '\'' +
                '}';
    }
}
