package com.classwork.class20181205Stack;

public interface Stack {

    int size();
    void addElement(String value);
    boolean isEmpty();
    String getElement();

}
