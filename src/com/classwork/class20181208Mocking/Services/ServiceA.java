package com.classwork.class20181208Mocking.Services;

public class ServiceA {

    private ServiceB service;

    String generateResult() {
        String serviceResult = service.run();
        return serviceResult + " " + "serviceA";
    }

    public ServiceB getService() {
        return service;
    }

    public void setService(ServiceB service) {
        this.service = service;
    }

}
