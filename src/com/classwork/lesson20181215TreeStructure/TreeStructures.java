package com.classwork.lesson20181215TreeStructure;

import java.util.*;

public class TreeStructures {

    public static void main(String[] args) {

        Set<User> userSet = new TreeSet();

        List<User> userList = Arrays.asList(
                new User("vasia", "pupkin", 39),
                new User("petia", "vasechkin", 33),
                new User("porfirii", "ivanov", 43),
                new User("alex", "petrov", 43),
                new User("kolia", "vasin", 35)

        );

        userSet.addAll(userList);
        System.out.println(userSet);

        for (User user: userList) {
            System.out.println(user);
        }

        NavigableSet<User> sortedUsers = new TreeSet(userSet);

        Set<User> youngUsers = sortedUsers.headSet(new User("", "", 15), true);

        for (User user: youngUsers) {
            System.out.println(user);
        }

        Set<User> oldUsers = sortedUsers.tailSet(new User("", "", 40), true);

        for (User user: oldUsers) {
            System.out.println(user);
        }

    }
}
