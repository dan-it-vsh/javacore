package com.classwork.lesson20181215HashMaps;

import java.util.*;

public class HashStructures {

    public static void main(String[] args) {

        Map<User, List<String>> hashMap = new HashMap();

        hashMap.put(new User("vasia", "pupkin", 39), Arrays.asList("admin", "user"));
        hashMap.put(new User("petia", "vasechkin", 33), Arrays.asList("designer", "user"));
        hashMap.put(new User("porfirii", "ivanov", 43), Arrays.asList("user"));
        hashMap.put(new User("alex", "petrov", 43),Arrays.asList("user"));
        hashMap.put(new User("kolia", "vasin", 35),Arrays.asList("user"));

//        Set entrySet = hashMap.entrySet();
//        for (Object elem : entrySet) {
//            Map.Entry mapEntry = (Map.Entry) elem;
//            System.out.print("key is: " + mapEntry.getKey() + " & Value is: ");
//            System.out.println(mapEntry.getValue().toString());
//        }

        for (Map.Entry<User, List<String>> entry: hashMap.entrySet()) {
            System.out.println(entry.getKey().getName() + " = " + entry.getValue());
        }

        for (User user: hashMap.keySet()) {
            System.out.println(user.getName());
        }

        for (List<String> roles: hashMap.values()) {
            System.out.println(roles);
        }

        List<String> strs = Arrays.asList("sss", "sdas", "a", "dsa");

        System.out.println(aggregateValues(strs));


    }

    public static Map<String, Integer> aggregateValues(List<String> values){

        Map<String, Integer> result = new HashMap();

        int index = 0;

        for (String value: values) {

            if (result.get(value) == null)
                result.put(value, 1);
            else
                result.put(value, result.get(value) + 1);

        }

        return result;
    }

}
