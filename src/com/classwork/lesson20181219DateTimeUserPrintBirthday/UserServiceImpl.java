package com.classwork.lesson20181219DateTimeUserPrintBirthday;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class UserServiceImpl implements UserService {

    private final static String DATE_FORMAT = "dd/MM/yyyy";

    @Override
    public void printBirthDay (User user) {

        // SimpleDateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        System.out.println(dateFormat.format(user.getBirthDate()));

        // DateTime formatter
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        Instant instant = Instant.now();


    }

}
