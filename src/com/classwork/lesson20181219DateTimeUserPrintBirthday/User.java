package com.classwork.lesson20181219DateTimeUserPrintBirthday;


import com.homework.hw08.DaysOfWeek;

import java.util.Date;

public class User {

    private String name;
    private Date birthDate;
    private Date salaryDate;
    private int salary;

    public User(String name, Date birthdate) {
        this.name = name;
        this.birthDate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(Date salaryDate) {
        this.salaryDate = salaryDate;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
