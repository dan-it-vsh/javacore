package com.classwork.class20181215Set;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Company company = new Company(Arrays.asList(
                new Employee(37, "Vasia"),
                new Employee(28, "Petia"),
                new Employee(25, "Vania")
        ));

        for (Employee employee: company) {
            System.out.println(employee.getName());
        }

        for (Employee employee: company) {
            System.out.println(employee.getName());
        }

    }
}
