package com.classwork.class20181215Set;

import java.util.Iterator;
import java.util.List;

public class Company implements Iterable<Employee> {

    private final List<Employee> employees;

//    private Iterator<Employee> iterator;

    Company(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public Iterator<Employee> iterator() {
        return new Iterator<Employee>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < employees.size();
            }

            @Override
            public Employee next() {
                return employees.get(index++);
            }
        };
    }
}
