package com.classwork.lesson20181217Date.TestsWithDate;

public interface lesson20181217SimpleHashMap<K,V> {

    void put(K key, V Value);
    V get (K key);
    boolean containsKey(K key);
    boolean containsValue(V value);

}
