package com.classwork.lesson20181217Date.TestsWithDate;

import java.text.SimpleDateFormat;
import java.util.Date;


public class myTest01SimpleDateFormat {

    public static void main(String[] args) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        System.out.println("date: " + dateFormat.format(new Date()));
    }
}
