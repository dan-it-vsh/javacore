package com.classwork.class20181205Stack;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class MyStackTest {

    // size : int
    // isEmpty : boolean
    // addElement : void
    // deleteElement
    // isExists

    @Test
    public void ShouldReturnTrueWhenAddedTwoElements() {

        // given
        MyStack stack = new MyStack();

        // when
        stack.addElement("element1");
        stack.addElement("element2");

        // then
        assertThat(stack.size(), is(2));

    }

    @Test
    public void ShouldReturnNullWhenStackIsEmpty() {

        // givem
        MyStack stack = new MyStack();
        // when
//        stack.getElement();
        // then
        assertThat(stack.getElement(), is(nullValue()));


    }

    @Test
    public void ShouldReturnElementWhenAddedElemtn() {

        // givem
        MyStack stack = new MyStack();

        // when
        stack.addElement("elementw2");

        // then
        String expectedElement = "elementw2";
        String result = stack.getElement();
        assertThat(result, is(expectedElement));


    }

    @Test
    public void ShouldReturnElementAndDeleteFromStack() {

        // givem
        MyStack stack = new MyStack();

        // when
        stack.addElement("value");

        // then
        String expectedElement = "value";
        String result = stack.getElement();
        assertThat(result, is(expectedElement));
        assertThat(stack.size(), is(0));
    }

    @Test
    public void ShouldCheckTwoAddedElementForEquality() {

        // givem
        MyStack stack = new MyStack();

        // when
        stack.addElement("value");
        stack.addElement("value2");

        // then
        String[] expectedResult = {"value", "value2"};
        String[] result = {stack.getElement(), stack.getElement()};
        Assert.assertArrayEquals(result, expectedResult);

    }

}


