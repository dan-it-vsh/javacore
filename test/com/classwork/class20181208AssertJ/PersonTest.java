package com.classwork.class20181208AssertJ;

//import org.junit.Assert;

import org.junit.Test;

//import static org.junit.Assert.*;
import org.assertj.core.api.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class PersonTest {

    @Test
    public void checkPersonName() {
        // given
        Person person = new Person("Alexander");

        // when

        // then
//        String name = alexander.getName();
//        Assert.assertTrue(name.startsWith("Alex"));

        assertThat(person.getName())
                .startsWith("Alex")
                .endsWith("er")
                .contains("e");

    }

    @Test
    public void checkPersonsCars() {
        // given
        Person person = new Person("Alexander");
        person.setCars(new String[]{"bmw", "mercedes", "opel"});

        // when

        // then

        assertThat(person.getCars())
                .contains("bmw");

    }

    @Test
    public void checkPersonsNameField() {
        // given
        Person person = new Person("Alexander");
        String expectedField = "Alexander";

        // when

        // then

        assertThat(person)
                .extracting("name")
                .contains(expectedField);

    }

}