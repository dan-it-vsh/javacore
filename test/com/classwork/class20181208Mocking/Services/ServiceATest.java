package com.classwork.class20181208Mocking.Services;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServiceATest {

    @Test
    public void shouldGenerateRightResult() {
        // params
        // mocks
        ServiceB serviceB = mock(ServiceB.class);
        when(serviceB.run()).thenReturn("serviceB");

        // class to test
        ServiceA serviceA = new ServiceA();
        serviceA.setService(serviceB);

        // method invocation
        String result = serviceA.generateResult();

        //asserts
        Assertions.assertThat(result).isEqualTo("serviceB serviceA");


    }

//    @Test
//    public void getService() {
//    }
//
//    @Test
//    public void setService() {
//    }
}