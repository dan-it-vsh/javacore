package com.homework.hw11;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class PetTest {

    private Species species = Species.FISH;
    private String nickname = "Sharik";
    private int age = 2;
    private int trickLevel = 0;
    private int legsNumber = 0;
    private HashSet<String> habits = null;

    @Test
    public void petToSringReturnsExpectedString() {

        String expectedResult =
                "FISH{nickname=\'" + nickname + "\', age=" + age +
                        "," + " trickLevel=" + trickLevel + "," +
                        " \'can fly\'=" + false + "," +
                        " \'has fur\'=" + false + "," +
                        " \'number of legs\'=" + legsNumber + "," +
                        " habits=" + null + "}";

        Pet pet1 = new Fish(nickname, age, trickLevel,habits);
        Assert.assertEquals(expectedResult, pet1.toString());

    }

    @Test
    public void petToSringReturnsUnexpectedString() {

        String expectedResult =
                "FISH{nickname=\'" + nickname + "\', age=" + age +
                        "," + " trickLevel=" + trickLevel + "," +
                        " \'can fly\'=" + false + "," +
                        " \'has fur\'=" + false + "," +
                        " \'number of legs\'=" + legsNumber + "," +
                        " habits=" + null + "}";

        expectedResult = "P" + expectedResult;

        Pet pet1 = new Fish(nickname, age, trickLevel, habits);
        Assert.assertNotEquals(expectedResult, pet1.toString());

    }

}
