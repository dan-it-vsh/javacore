package com.homework.hw18;

import com.homework.hw18.DAO.CollectionFamilyDao;
import com.homework.hw18.Families.Family;
import com.homework.hw18.Humans.Human;
import com.homework.hw18.Humans.Man;
import com.homework.hw18.Humans.Woman;
import com.homework.hw18.Pets.Dog;
import com.homework.hw18.Pets.DomesticCat;
import com.homework.hw18.Pets.Fish;
import com.homework.hw18.Pets.RoboCat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public class CollectionFamilyDaoTest {

    private Dog dog1 = new Dog("Sharik", 2, 20, new HashSet(Arrays.asList("eating", "sleepimg")));
    private RoboCat robCat1 = new RoboCat("Murzik", 1, 90, new HashSet(Arrays.asList("eating", "sleeping", "running")));
    private Fish fish1 = new Fish("Ponchik", 3, 30, new HashSet(Arrays.asList("swimming")));
    private DomesticCat domCat1 = new DomesticCat("Vaska", 3, 30, new HashSet(Arrays.asList("lazy", "eating")));

    private Man father1 = new Man("Petr", "Ivanov", 219186001976L, 100, null, null);
    private Man father2 = new Man("Ivan", "Sidorov", 263595601978L, 107, null, null);
    private Man father3 = new Man("Panas", "Petrov", 374533201981L, 111, null, null);
    private Man father4 = new Man("Chingiz", "Khanov", -8478001969L, 114, null, null);
    private Woman mother1 = new Woman("Fekla", "Ivanova", 296514001979L, 99, null, null);
    private Woman mother2 = new Woman("Paraska", "Sidorova", 348354001981L, 113, null, null);
    private Woman mother3 = new Woman("Marfa", "Petrova", 425246401983L, 102, null, null);
    private Woman mother4 = new Woman("Vama", "Khanova", 186008401975L, 103, null, null);

    private Family family1 = new Family(mother1, father1);
    private Family family2 = new Family(mother2, father2);
    private Family family3 = new Family(mother3, father3);
    private Family family4 = new Family(mother4, father4);

    private CollectionFamilyDao familiesCollection = new CollectionFamilyDao();

    @Before
    public void setupFamilies() {

        for (int i = 0; i < 3; i++) {
            family1.bornChild("","");
            family2.bornChild("","");
            family3.bornChild("","");
            family4.bornChild("","");
        }

        family1.getPets().add(dog1);
        family2.getPets().add(fish1);
        family3.getPets().add(domCat1);

        familiesCollection.saveFamily(family1); // index 0
        familiesCollection.saveFamily(family2); // index 1
        familiesCollection.saveFamily(family3); // index 2
        familiesCollection.saveFamily(family4); // index 3

    }

    @Test
    public void getAllFamiliesReturnsEmptyListWhenNoFamilies() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        List<Family> result = familiesCollection1.getAllFamilies();

        Assert.assertEquals(result.size(), 0);

    }

    @Test
    public void getAllFamiliesReturnsNonEmptyListWhenFamiliesAdded() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1);
        familiesCollection1.saveFamily(family2);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family4);

        List<Family> result = familiesCollection1.getAllFamilies();

        Assert.assertEquals(result.size(), 4);

    }


    @Test
    public void getFamilyByIndexReturnsFamilyWhenIndexExists() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3


        Family result = familiesCollection1.getFamilyByIndex(0);
        Assert.assertEquals(result, family1);

        result = familiesCollection1.getFamilyByIndex(1);
        Assert.assertEquals(result, family2);

        result = familiesCollection1.getFamilyByIndex(2);
        Assert.assertEquals(result, family3);

        result = familiesCollection1.getFamilyByIndex(2);
        Assert.assertNotEquals(result, family2);

    }

    @Test
    public void getFamilyByIndexReturnsNullWhenIndexDoesNotExist() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        Family result = familiesCollection1.getFamilyByIndex(5);
        Assert.assertNull(result);

        result = familiesCollection1.getFamilyByIndex(1);
        Assert.assertNotNull(result);

    }

    @Test
    public void deleteFamilyByIndexReturnsTrueIfFamilyDeleted() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        boolean result = familiesCollection1.deleteFamily(1);
        Assert.assertTrue(result);

    }

    @Test
    public void deleteFamilyByIndexReturnsFalseIfWrongIndex() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        boolean result = familiesCollection1.deleteFamily(5);
        Assert.assertFalse(result);

    }

    @Test
    public void deleteFamilyByObjectReturnsTrueIfFamilyDeleted() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        boolean result = familiesCollection1.deleteFamily(family1);
        Assert.assertTrue(result);

        result = familiesCollection1.deleteFamily(family2);
        Assert.assertTrue(result);

        result = familiesCollection1.deleteFamily(family3);
        Assert.assertTrue(result);

        result = familiesCollection1.deleteFamily(family4);
        Assert.assertTrue(result);

    }

    @Test
    public void deleteFamilyByObjectReturnsFalseIfWrongFamily() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        boolean result = familiesCollection1.deleteFamily(family1);
        Assert.assertTrue(result);

        result = familiesCollection1.deleteFamily(family1); // already deleted
        Assert.assertFalse(result);

    }

    @Test
    public void saveFamilyAddedFamilyIfFamilyDoesNotExist() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1);
        familiesCollection1.saveFamily(family2);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family4);

        List<Family> result = familiesCollection1.getAllFamilies();

        Assert.assertEquals(result.size(), 4);

    }

    @Test
    public void saveFamilyOverWriteFamilyIfFamilyExists() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        familiesCollection1.saveFamily(family1); // index 0
        familiesCollection1.saveFamily(family2); // index 1
        familiesCollection1.saveFamily(family3); // index 2
        familiesCollection1.saveFamily(family4); // index 3

        Family result1 = familiesCollection1.getFamilyByIndex(0);

        // family5 equals family1 because the same parents
        Family family5 = new Family(family1.getFather(), family1.getMother());
        Family family6 = new Family(family1.getFather(), family2.getMother());

        family5.getPets().add(domCat1);
        family5.getPets().add(robCat1);
        family5.addChild(new Human("Vasia", "Pupkin", 1291586402010L, 100, null, family1));

        Assert.assertEquals(familiesCollection1.numberOfFamilies(), 4);
        familiesCollection1.saveFamily(family5);
        Assert.assertEquals(familiesCollection1.numberOfFamilies(), 4);
        familiesCollection1.saveFamily(family6);
        Assert.assertEquals(familiesCollection1.numberOfFamilies(), 5);

        Family result2 = familiesCollection1.getFamilyByIndex(0);

        Assert.assertEquals(result1, result2);
        Assert.assertNotEquals(result1.toString(), result2.toString());

    }

    @Test
    public void numberOfFamiliesIncreasesWhenFamiliesAdded() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();

        int result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 0);

        familiesCollection1.saveFamily(family1);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 1);

        familiesCollection1.saveFamily(family2);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 2);

        familiesCollection1.saveFamily(family3);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 3);

        familiesCollection1.saveFamily(family3); // family already exists
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 3);

        familiesCollection1.saveFamily(family4);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 4);

    }

    @Test
    public void numberOfFamiliesDecreasesWhenFamiliesDeleted() {

        CollectionFamilyDao familiesCollection1 = new CollectionFamilyDao();
        familiesCollection1.saveFamily(family1);
        familiesCollection1.saveFamily(family2);
        familiesCollection1.saveFamily(family3);
        familiesCollection1.saveFamily(family4);

        int result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 4);

        familiesCollection1.deleteFamily(family1);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 3);

        familiesCollection1.deleteFamily(family1); // family already deleted
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 3);

        familiesCollection1.deleteFamily(10); // wrong family index
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 3);

        familiesCollection1.deleteFamily(0);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 2);

        familiesCollection1.deleteFamily(0);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 1);

        familiesCollection1.deleteFamily(0);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 0);

        familiesCollection1.deleteFamily(0);
        result = familiesCollection1.numberOfFamilies();
        Assert.assertEquals(result, 0);

    }

}