package com.homework.hw18;

import com.homework.hw18.Humans.Human;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class HumanTest {

    private final String DATE_FORMAT = "dd/MM/yyyy";
    private final String TIME_ZONE = "Europe/Kiev";

    private String name = "Vasia";
    private String surname = "Pubkin";
    private long birthDate = 958251602000L;
    private int iq = 0;

    @Test
    public void humanToSringReturnsExpectedString() {

        String expectedResult =
                "Human{name=\'" + name + "\', surname=\'" + surname +
                        "\', birthdate=\'" +
                        Instant.ofEpochMilli(birthDate).atZone(ZoneId.of(TIME_ZONE)).toLocalDateTime().format(DateTimeFormatter.ofPattern(DATE_FORMAT)) +
                        "\', iq=" + iq +
                        ", schedule=" + null + "}";

        Human human1 = new Human(name, surname, birthDate, 0, null, null);
        Assert.assertEquals(expectedResult, human1.toString());
    }

    @Test
    public void humanToSringReturnsUnexpectedString() {

        String expectedResult =
                "Human{name=\'" + name + "\', surname=\'" + surname +
                        "\', birthdate=" +
                        Instant.ofEpochMilli(birthDate).atZone(ZoneId.of(TIME_ZONE)).toLocalDateTime().format(DateTimeFormatter.ofPattern(DATE_FORMAT)) +
                        ", iq=" + iq +
                        ", schedule=" + null + "}";

        expectedResult = "H" + expectedResult;

        Human human1 = new Human(name, surname, birthDate, 0, null, null);
        Assert.assertNotEquals(expectedResult, human1.toString());
    }

    @Test
    public void hashCodesEqualWhileObjectsAreEqual() {

        Human human1 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);



        Assert.assertEquals(human1.hashCode(), human2.hashCode());
        Assert.assertTrue(human1.equals(human2));

    }

    @Test
    public void hashCodesNotEqualWhileObjectsAreNotEqual() {

        Human human1 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);
        Human human2 = new Human("asiaV", "Pupkin", 1266098402010L, 0, null, null);

        Assert.assertNotEquals(human1.hashCode(),human2.hashCode());
        Assert.assertNotEquals(human1,human2);

    }

    // equals contract tests
    @Test
    public void objectEqualsToHimself() {

        // Object equals to himself

        Human human1 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);

        Assert.assertTrue(human1.equals(human1));

    }

    @Test
    public void aEqualsBWhenBEqualsA() {

        // if A equals B then B should equal A

        Human human1 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);

        Assert.assertTrue(human1.equals(human2));
        Assert.assertTrue(human2.equals(human1));

    }

    @Test
    public void aEqualsCWhenAEqualsBAndBEqualsC() {

        Human human1 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);
        Human human3 = new Human("Vasia", "Pupkin", 1266098402010L, 0, null, null);


        Assert.assertTrue(human1.equals(human2));
        Assert.assertTrue(human2.equals(human3));
        Assert.assertTrue(human1.equals(human3));

    }


}
