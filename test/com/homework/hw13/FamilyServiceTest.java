package com.homework.hw13;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FamilyServiceTest {

    private Dog dog1 = new Dog("Sharik", 2, 20, new HashSet(Arrays.asList("eating", "sleepimg")));
    private RoboCat robCat1 = new RoboCat("Murzik", 1, 90, new HashSet(Arrays.asList("eating", "sleeping", "running")));
    private Fish fish1 = new Fish("Ponchik", 3, 30, new HashSet(Arrays.asList("swimming")));
    private DomesticCat domCat1 = new DomesticCat("Vaska", 3, 30, new HashSet(Arrays.asList("lazy", "eating")));

    private Man father1 = new Man("Petr", "Ivanov", 1976, 100, null, null);
    private Man father2 = new Man("Ivan", "Sidorov", 1978, 107, null, null);
    private Man father3 = new Man("Panas", "Petrov", 1981, 111, null, null);
    private Man father4 = new Man("Chingiz", "Khanov", 1969, 114, null, null);
    private Woman mother1 = new Woman("Fekla", "Ivanova", 1979, 99, null, null);
    private Woman mother2 = new Woman("Paraska", "Sidorova", 1981, 113, null, null);
    private Woman mother3 = new Woman("Marfa", "Petrova", 1983, 102, null, null);
    private Woman mother4 = new Woman("Vama", "Khanova", 1975, 103, null, null);

    private Family family1 = new Family(father1, mother1);
    private Family family2 = new Family(father2, mother2);
    private Family family3 = new Family(father3, mother3);
    private Family family4 = new Family(father4, mother4);

    @Test
    public void getAllFamiliesReturnsEmptyListWhenNoFamiliesAdded() {

        FamilyService familiesService = new FamilyService();

        List<Family> result = familiesService.getAllFamilies();

        Assert.assertTrue(result.isEmpty());
        Assert.assertEquals(result.size(), 0);

    }

    @Test
    public void getAllFamiliesReturnsNonEmptyListWhenFamiliesAdded() {

        FamilyService familiesService = new FamilyService();

        List<Family> result = familiesService.getAllFamilies();
        Assert.assertTrue(result.isEmpty());
        Assert.assertEquals(result.size(), 0);

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);

        result = familiesService.getAllFamilies();

        Assert.assertFalse(result.isEmpty());
        Assert.assertEquals(result.size(), 3);

    }

    @Test
    public void getFamiliesBiggerThanReturnsTwoFamiliesWithMembersNumberBiggerThanThree() {

        FamilyService familiesService = new FamilyService();

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);
        familiesService.getAllFamilies().add(family4);

        List<Family> result = familiesService.getFamiliesBiggerThan(3);
        Assert.assertEquals(result.size(), 0);

        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram", "Papam", 2011, 99, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram2", "Papam2", 2012, 100, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram3", "Papam3", 2013, 101, null, null));

        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name1", "Surname1", 2007, 103, null, null));
        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name2", "Surname2", 2012, 107, null, null));
        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name3", "Surname3", 2015, 101, null, null));

        result = familiesService.getFamiliesBiggerThan(3);
        Assert.assertEquals(result.size(), 2);

        result = familiesService.getFamiliesBiggerThan(0);
        Assert.assertEquals(result.size(), 4);

    }

    @Test
    public void getFamiliesLessThanReturnsTwoFamiliesWithMembersNumberLessThanThree() {

        FamilyService familiesService = new FamilyService();

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);
        familiesService.getAllFamilies().add(family4);

        List<Family> result = familiesService.getFamiliesLessThan(3);
        Assert.assertEquals(result.size(), 4);

        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram", "Papam", 2011, 99, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram2", "Papam2", 2012, 100, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram3", "Papam3", 2013, 101, null, null));

        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name1", "Surname1", 2007, 103, null, null));
        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name2", "Surname2", 2012, 107, null, null));

        result = familiesService.getFamiliesLessThan(3);
        Assert.assertEquals(result.size(), 2);

        result = familiesService.getFamiliesLessThan(5);
        Assert.assertEquals(result.size(), 3);

        result = familiesService.getFamiliesLessThan(2);
        Assert.assertEquals(result.size(), 0);

    }

    @Test
    public void countFamiliesWithMemberNumber() {

        FamilyService familiesService = new FamilyService();

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);
        familiesService.getAllFamilies().add(family4);

        int result = familiesService.countFamiliesWithMemberNumber(2);
        Assert.assertEquals(result, 4);

        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram", "Papam", 2011, 99, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram2", "Papam2", 2012, 100, null, null));
        familiesService.getAllFamilies().get(0).getChildren().add(new Human("Haram3", "Papam3", 2013, 101, null, null));

        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name1", "Surname1", 2007, 103, null, null));
        familiesService.getAllFamilies().get(1).getChildren().add(new Human("Name2", "Surname2", 2012, 107, null, null));

        result = familiesService.countFamiliesWithMemberNumber(2);
        Assert.assertEquals(result, 2);

        result = familiesService.countFamiliesWithMemberNumber(3);
        Assert.assertEquals(result, 0);

        result = familiesService.countFamiliesWithMemberNumber(4);
        Assert.assertEquals(result, 1);

        result = familiesService.countFamiliesWithMemberNumber(5);
        Assert.assertEquals(result, 1);

    }

    @Test
    public void createNewFamilyIncreasesFamilyListSizeWhenNewFamilyCreated() {

        FamilyService familiesService = new FamilyService();

        int result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 0);

        familiesService.createNewFamily(father1, mother1);

        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 1);

        familiesService.createNewFamily(father2, mother2);

        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 2);

    }

    @Test
    public void createNewFamilyKeepsFamilyListSizeWhenFamilyAlreadyExists() {

        FamilyService familiesService = new FamilyService();

        int result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 0);

        familiesService.createNewFamily(father1, mother1);

        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 1);

        familiesService.createNewFamily(father1, mother1);

        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 1);

    }

    @Test
    public void deleteFamilyByIndexDescreasesFamilyListSizeWhenDeletedSuccessfully() {

        FamilyService familiesService = new FamilyService();

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);
        familiesService.getAllFamilies().add(family4);

        int result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 4);

        familiesService.deleteFamilyByIndex(0);
        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 3);

        familiesService.deleteFamilyByIndex(0);
        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 2);

    }

    @Test
    public void deleteFamilyByIndexKeepsFamilyListSizeWhenWrongIndex() {

        FamilyService familiesService = new FamilyService();

        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        familiesService.getAllFamilies().add(family3);
        familiesService.getAllFamilies().add(family4);

        int result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 4);

        familiesService.deleteFamilyByIndex(5);
        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 4);

        familiesService.deleteFamilyByIndex(-1);
        result = familiesService.getAllFamilies().size();
        Assert.assertEquals(result, 4);

    }

    @Test
    public void bornChildIncreasesChildrenListSize() {

        // plus checks for child's name and surname

        FamilyService familiesService = new FamilyService();
        familiesService.getAllFamilies().add(family1);

        // Children List size()
        int result = family1.getChildren().size();
        Assert.assertEquals(result, 0);

        // The same as above and to double check we work with correct Family object
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        Assert.assertEquals(result, 0);

        String nameMens = "Feofan", nameWomens = "Paraska";
        familiesService.bornChild(family1, nameMens, nameWomens);

        // Children List size() after born
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        Assert.assertEquals(result, 1);

        // Check for child's name
        String currName = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getName();
        Assert.assertTrue(currName.equals(nameMens) || currName.equals(nameWomens));

        // Check for child's surname
        String currSurname = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getSurname();
        Assert.assertEquals(currSurname,family1.getFather().getSurname());

    }

    @Test
    public void adoptChildIncreasesChildrenListSize() {

        // plus checks for child's name, surname, year, iq

        FamilyService familiesService = new FamilyService();
        familiesService.getAllFamilies().add(family1);

        // Children List size()
        int result = family1.getChildren().size();
        Assert.assertEquals(result, 0);

        // The same as above and to double check we work with correct Family object
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        Assert.assertEquals(result, 0);

        String name = "Feofan", surname = "Paramoniov";
        int year = 1999, iq = 99;
        familiesService.adoptChild(family1, new Human(name, surname, year, iq, null, family2));

        // Children List size() after adopting
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        Assert.assertEquals(result, 1);

        // Check for child's name
        String currName = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getName();
        Assert.assertEquals(currName, name);

        // Check for child's surname
        String currSurname = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getSurname();
        Assert.assertEquals(currSurname,family1.getFather().getSurname());

        // Check for child's year
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getYearBirthDay();
        Assert.assertEquals(result, year);

        // Check for child's iq
        result = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().get(0).getIq();
        Assert.assertEquals(result, iq);

    }

    @Test
    public void deleteAllChildrenOlderThenDecreasesChidrenListSizeInFamilyWhenSuccess() {

        FamilyService familiesService = new FamilyService();

        family1.getChildren().clear();
        family2.getChildren().clear();
        familiesService.getAllFamilies().add(family1);
        familiesService.getAllFamilies().add(family2);
        int result1 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        int result2 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family2)).getChildren().size();
        Assert.assertEquals(result1, 0);
        Assert.assertEquals(result2, 0);

        familiesService.adoptChild(familiesService.getFamilyById(0), new Human("name1", "surname1", 1995, 0, null,null));
        familiesService.adoptChild(familiesService.getFamilyById(0), new Human("name2", "surname2", 1997, 0, null,null));
        familiesService.adoptChild(familiesService.getFamilyById(1), new Human("name3", "surname3", 1993, 0, null,null));
        familiesService.adoptChild(familiesService.getFamilyById(1), new Human("name4", "surname4", 2010, 0, null,null));


        result1 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        result2 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family2)).getChildren().size();
        Assert.assertEquals(result1, 2);
        Assert.assertEquals(result2, 2);

        familiesService.deleteAllChildrenOlderThen(10);
        result1 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family1)).getChildren().size();
        result2 = familiesService.getAllFamilies().get(familiesService.getAllFamilies().indexOf(family2)).getChildren().size();
        Assert.assertEquals(result1, 0);
        Assert.assertEquals(result2, 1);

    }

    @Test
    public void countIsChangingWhenNewFamilyAddedOrExistingDeleted() {

        FamilyService familiesService = new FamilyService();

        int result = familiesService.count();
        Assert.assertEquals(result, 0);

        familiesService.createNewFamily(father1, mother1);
        result = familiesService.count();
        Assert.assertEquals(result, 1);

        familiesService.createNewFamily(father2, mother2);
        result = familiesService.count();
        Assert.assertEquals(result, 2);

        familiesService.deleteFamilyByIndex(0);
        result = familiesService.count();
        Assert.assertEquals(result, 1);

    }

    @Test
    public void countIsNotChangingWhenTryingAddExistingFamily() {

        FamilyService familiesService = new FamilyService();

        int result = familiesService.count();
        Assert.assertEquals(result, 0);

        familiesService.createNewFamily(father1, mother1);
        result = familiesService.count();
        Assert.assertEquals(result, 1);

        familiesService.createNewFamily(father2, mother2);
        result = familiesService.count();
        Assert.assertEquals(result, 2);

        familiesService.createNewFamily(father2, mother2);
        result = familiesService.count();
        Assert.assertEquals(result, 2);

    }

    @Test
    public void getFamilyByIdReturnsCorrectFamilyObject() {

        FamilyService familiesService = new FamilyService();

        familiesService.createNewFamily(father1, mother1); // index 0
        familiesService.createNewFamily(father2, mother2); // index 1
        familiesService.createNewFamily(father3, mother3);
        familiesService.createNewFamily(father4, mother4);

        Family currFamily = familiesService.getFamilyById(1);

        Human result = currFamily.getFather();
        Assert.assertEquals(result, father2);

        result = currFamily.getMother();
        Assert.assertEquals(result, mother2);
        Assert.assertNotEquals(result, mother3);

    }

    @Test
    public void getPetsReturnListWithNonZeroSizeIfPetsExist() {

        FamilyService familiesService = new FamilyService();

        familiesService.createNewFamily(father1,mother1); // index 0

        int result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 0);

        familiesService.getFamilyById(0).getPets().add(dog1);
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 1);

        familiesService.getFamilyById(0).getPets().add(domCat1);
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 2);

        familiesService.getFamilyById(0).getPets().add(robCat1);
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 3);

    }

    @Test
    public void addPetIncreasesPetsListSizeWhenPetAddedSuccessfully() {

        FamilyService familiesService = new FamilyService();

        familiesService.createNewFamily(father1,mother1); // index 0

        int result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 0);

        familiesService.addPet(0, dog1);
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 1);

        familiesService.addPet(0, robCat1);
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 2);

    }

    @Test
    public void addPetKeepsPetsListSizeWhenPetAlreadyExists() {

        // Pet exists if nickname and age are the same

        FamilyService familiesService = new FamilyService();

        familiesService.createNewFamily(father1,mother1); // index 0

        int result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 0);

        String nickname1 = "Sharik", nickname2 = "Murzik";
        int age = 5, trickLevel1 = 45, trickLevel2 = 55;
        Set<String> habits1 = new HashSet(Arrays.asList("lazy", "eating", "playing"));
        Set<String> habits2 = new HashSet(Arrays.asList("fouling", "playing"));

        familiesService.addPet(0, new Dog(nickname1, age, trickLevel1, habits1));
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 1);

        familiesService.addPet(0, new Dog(nickname1, age, trickLevel2, habits2));
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 1);

        familiesService.addPet(0, new Dog(nickname2, age, trickLevel2, habits2));
        result = familiesService.getPets(0).size();
        Assert.assertEquals(result, 2);

    }
}