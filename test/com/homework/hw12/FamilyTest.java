package com.homework.hw12;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class FamilyTest {

    private String name1 = "Vasia", name2 = "KHrystia";
    private String surname1 = "Pupkin", surname2 = "Pupkina";
    private int year1 = 1980, year2 = 1983;
    int iq = 0;

    private String name3 = "Sharik", name4 = "Paraska", name5 = "Petia";
    private String surname3 = "Pupkin", surname4 = "Pupkina", surname5 = "Pupkin";
    private int year3 = 2001, year4 = 2005, year5 = 2010;

    private Human human1 = new Human(name1, surname1, year1, 101, null, null);
    private Human human2 = new Human(name2, surname2, year2, 102, null, null);
    private Human child1 = new Human(name3, surname3, year3, 103, null, null);
    private Human child2 = new Human(name4, surname4, year4, 104, null, null);
    private Human child3 = new Human(name5, surname5, year5, 105, null, null);

    @Test
    public void familyToStringReturnsExpectedString() {

        String expectedResult =
                "Family{mother=\'" + name1 + " " + surname1 + "\'" +
                        " father=\'" + name2 + " " + surname2 + "\'" +
                        " children=\'" + "[]" + "\'" +
                        " pets=\'" + "[]" + "\'}";

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(expectedResult, family1.toString());

    }

    @Test
    public void familyToStringReturnsUnxpectedString() {

        String expectedResult =
                "Family{mother=\'" + name1 + " " + surname1 + "\'" +
                        " father=\'" + name2 + " " + surname2 + "\'" +
                        " children=\'" + "[]" + "\'" +
                        " pets=\'" + "[]" + "\'}";

        expectedResult = "F" + expectedResult;

        Family family1 = new Family(human1, human2);

        Assert.assertNotEquals(expectedResult, family1.toString());
    }

    @Test
    public void deleteChildByIndexDescreasesChildrenArrayListSizeByOneWhenSuccessful() {

        // Array Children will get decreased length
        // after successful deleteChild(Index)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        family1.deleteChild(2);

        Assert.assertEquals(family1.getChildren().size(), 2);

    }

    @Test
    public void deleteChildByIndexReturnsTrueWhenSuccessful() {

        // deleteChild returns true
        // after successful deleteChild(Index)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        boolean result = family1.deleteChild(2);

        Assert.assertTrue(result);

    }

    @Test
    public void deleteChildByIndexKeepsChildrenArrayListUnchangedWhenUnsuccessful() {

        // Array Children will remain the same
        // if deleteChild(Index) uses a wrong Index

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        ArrayList<Human> childrenTmp = new ArrayList(family1.getChildren().size());

        for (int i = 0; i < family1.getChildren().size(); i++) {

            childrenTmp.add(new Human(
                    family1.getChildren().get(i).getName(),
                    family1.getChildren().get(i).getSurname(),
                    family1.getChildren().get(i).getYearBirthDay(),
                    family1.getChildren().get(i).getIq(),
                    family1.getChildren().get(i).getSchedule(),
                    family1.getChildren().get(i).getFamily()
            ));
        }

        family1.deleteChild(family1.getChildren().size() + 20);

        Assert.assertEquals(family1.getChildren().size(), 3);
        Assert.assertEquals(childrenTmp, family1.getChildren());

    }

    @Test
    public void deleteChildByIndexReturnsFalseWhenUnsuccessful() {

        // Returns false
        // if deleteChild(Index) uses a wrong Index

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        boolean result = family1.deleteChild(family1.getChildren().size() + 20);

        Assert.assertFalse(result);

    }

    @Test
    public void deleteChildByObjectDescreasesChildrenArrayListSizeByOneWhenSuccessful() {

        // Array Children will get decreased length
        // after successful deleteChild(Object)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        family1.deleteChild(child1);

        Assert.assertEquals(family1.getChildren().size(), 2);

    }

    @Test
    public void deleteChildByObjectReturnsTrueWhenSuccessful() {

        // deleteChild(Object) returns True
        // after successful deleteChild(Object)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        boolean result = family1.deleteChild(child1);

        Assert.assertTrue(result);

    }

    @Test
    public void deleteChildByObjectKeepsChildrenArrayListUnchangedWhenUnsuccessful() {

        // Array Children will remain the same
        // if deleteChild(Object) uses a wrong Object

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        ArrayList<Human> children1 = new ArrayList(family1.getChildren().size());

        for (int i = 0; i < family1.getChildren().size(); i++) {
            children1.add(new Human(
                    family1.getChildren().get(i).getName(),
                    family1.getChildren().get(i).getSurname(),
                    family1.getChildren().get(i).getYearBirthDay(),
                    family1.getChildren().get(i).getIq(),
                    family1.getChildren().get(i).getSchedule(),
                    family1.getChildren().get(i).getFamily()
            ));
        }

        family1.deleteChild(human1);

        Assert.assertEquals(family1.getChildren().size(), 3);
        Assert.assertEquals(children1, family1.getChildren());

    }

    @Test
    public void deleteChildByObjectReturnsFalseWhenUnsuccessful() {

        // deleteChild returns false
        // if deleteChild(Object) uses a wrong Object

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);

        boolean result = family1.deleteChild(human1);

        Assert.assertFalse(result);

    }

    @Test
    public void addChildIncreasesChildrenArrayListSizeByOneWhenSuccessful() {

        // Array Children increases length
        // after successful addChild(Object)

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(family1.getChildren().size(), 0);

        family1.addChild(child1);
        Assert.assertEquals(family1.getChildren().size(), 1);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1), child1);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1).getFamily(), family1);

        family1.addChild(child2);
        Assert.assertEquals(family1.getChildren().size(), 2);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().indexOf(child2)), child2);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1).getFamily(), family1);

        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().size(), 3);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().indexOf(child3)), child3);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1).getFamily(), family1);

    }

    @Test
    public void countFamilyIncreasesByOneWhenAddChildSuccessful() {

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(family1.countFamily(), 2);

        family1.addChild(child1);

        Assert.assertEquals(family1.countFamily(), 3);

        family1.addChild(child2);

        Assert.assertEquals(family1.countFamily(), 4);

        family1.addChild(child3);

        Assert.assertEquals(family1.countFamily(), 5);

        family1.deleteChild(child3);

        Assert.assertEquals(family1.countFamily(), 4);

        family1.deleteChild(child3);

        Assert.assertEquals(family1.countFamily(), 4);

    }


}
