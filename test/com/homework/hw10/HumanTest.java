package com.homework.hw10;

import org.junit.Assert;
import org.junit.Test;

public class HumanTest {

    private String name = "Vasia";
    private String surname = "Pubkin";
    private int year = 2000;
    private int iq = 0;

    @Test
    public void humanToSringReturnsExpectedString() {

        String expectedResult =
                "Human{name=\'" + name + "\', surname=\'" + surname +
                        "\', year=" + year + ", iq=" + iq +
                        ", schedule=" + null + "}";

        Human human1 = new Human(name, surname, year, iq, null, null);
        Assert.assertEquals(expectedResult, human1.toString());
    }

    @Test
    public void humanToSringReturnsUnexpectedString() {

        String expectedResult =
                "Human{name=\'" + name + "\', surname=\'" + surname +
                        "\', year=" + year + ", iq=" + iq +
                        ", schedule=" + null + "}";

        expectedResult = "H" + expectedResult;

        Human human1 = new Human(name, surname, year, iq, null, null);
        Assert.assertNotEquals(expectedResult, human1.toString());
    }

    @Test
    public void hashCodesEqualWhileObjectsAreEqual() {

        Human human1 = new Human("Vasia", "Pupkin", 2010, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 2010, 0, null, null);


        Assert.assertEquals(human1.hashCode(), human2.hashCode());
        Assert.assertTrue(human1.equals(human2));

    }

    @Test
    public void hashCodesNotEqualWhileObjectsAreNotEqual() {

        Human human1 = new Human("Vasia", "Pupkin", 2010, 0, null, null);
        Human human2 = new Human("asiaV", "Pupkin", 2010, 0, null, null);

        Assert.assertNotEquals(human1.hashCode(), human2.hashCode());
        Assert.assertNotEquals(human1, human2);

    }

    // equals contract tests
    @Test
    public void objectEqualsToHimself() {

        // Object equals to himself

        Human human1 = new Human("Vasia", "Pupkin", 2010, 0, null, null);

        Assert.assertTrue(human1.equals(human1));

    }

    @Test
    public void aEqualsBWhenBEqualsA() {

        // if A equals B then B should equal A

        Human human1 = new Human("Vasia", "Pupkin", 2010, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 2010, 0, null, null);

        Assert.assertTrue(human1.equals(human2));
        Assert.assertTrue(human2.equals(human1));

    }

    @Test
    public void aEqualsCWhenAEqualsBAndBEqualsC() {

        Human human1 = new Human("Vasia", "Pupkin", 2010, 0, null, null);
        Human human2 = new Human("Vasia", "Pupkin", 2010, 0, null, null);
        Human human3 = new Human("Vasia", "Pupkin", 2010, 0, null, null);


        Assert.assertTrue(human1.equals(human2));
        Assert.assertTrue(human2.equals(human3));
        Assert.assertTrue(human1.equals(human3));

    }


}
