package com.homework.hw10;

import org.junit.Assert;
import org.junit.Test;

public class FamilyTest {

    private String name1 = "Vasia", name2 = "KHrystia";
    private String surname1 = "Pupkin", surname2 = "Pupkina";
    private int year1 = 1980, year2 = 1983;
    int iq = 0;

    private String name3 = "Sharik", name4 = "Paraska", name5 = "Petia";
    private String surname3 = "Pupkin", surname4 = "Pupkina", surname5 = "Pupkin";
    private int year3 = 2001, year4 = 2005, year5 = 2010;

    private Human human1 = new Human(name1, surname1, year1, 0, null, null);
    private Human human2 = new Human(name2, surname2, year2, 0, null, null);
    private Human child1 = new Human(name3, surname3, year3, 0, null, null);
    private Human child2 = new Human(name4, surname4, year4, 0, null, null);
    private Human child3 = new Human(name5, surname5, year5, 0, null, null);

    @Test
    public void familyToStringReturnsExpectedString() {

        String expectedResult =
                "Family{mother=\'" + name1 + " " + surname1 + "\'" +
                        " father=\'" + name2 + " " + surname2 + "\'" +
                        " children=\'" + "[]" + "\'" +
                        " pet=\'" + null + "\'}";

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(expectedResult, family1.toString());

    }

    @Test
    public void familyToStringReturnsUnxpectedString() {

        String expectedResult =
                "Family{mother=\'" + name1 + " " + surname1 + "\'" +
                        " father=\'" + name2 + " " + surname2 + "\'" +
                        " children=\'" + "[]" + "\'" +
                        " pet=\'" + null + "\'}";

        expectedResult = "F" + expectedResult;

        Family family1 = new Family(human1, human2);

        Assert.assertNotEquals(expectedResult, family1.toString());
    }

    @Test
    public void deleteChildByIndexDescreasesChildrenArrayLengthByOneWhenSuccessful() {

        // Array Children will get decreased length
        // after successful deleteChild(Index)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        family1.deleteChild(2);

        Assert.assertEquals(family1.getChildren().length, 2);

    }

    @Test
    public void deleteChildByIndexReturnsTrueWhenSuccessful() {

        // deleteChild returns true
        // after successful deleteChild(Index)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        boolean result = family1.deleteChild(2);

        Assert.assertTrue(result);

    }

    @Test
    public void deleteChildByIndexKeepsChildrenArraysUnchangedWhenUnsuccessful() {

        // Array Children will remain the same
        // if deleteChild(Index) uses a wrong Index

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        Human[] children1 = new Human[family1.getChildren().length];

        for (int i = 0; i < children1.length; i++) {
            children1[i] = new Human(
                    family1.getChildren()[i].getName(),
                    family1.getChildren()[i].getSurname(),
                    family1.getChildren()[i].getYearBirthDay(),
                    family1.getChildren()[i].getIq(),
                    family1.getChildren()[i].getSchedule(),
                    family1.getChildren()[i].getFamily()
            );
        }

        family1.deleteChild(family1.getChildren().length + 20);

        Assert.assertEquals(family1.getChildren().length, 3);
        Assert.assertArrayEquals(children1, family1.getChildren());

    }

    @Test
    public void deleteChildByIndexReturnsFalseWhenUnsuccessful() {

        // Returns false
        // if deleteChild(Index) uses a wrong Index

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        boolean result = family1.deleteChild(family1.getChildren().length + 20);

        Assert.assertFalse(result);

    }

    @Test
    public void deleteChildByObjectDescreasesChildrenArrayLengthByOneWhenSuccessful() {

        // Array Children will get decreased length
        // after successful deleteChild(Object)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        family1.deleteChild(child1);

        Assert.assertEquals(family1.getChildren().length, 2);

    }

    @Test
    public void deleteChildByObjectReturnsTrueWhenSuccessful() {

        // deleteChild(Object) returns True
        // after successful deleteChild(Object)

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        boolean result = family1.deleteChild(child1);

        Assert.assertTrue(result);

    }

    @Test
    public void deleteChildByObjectKeepsChildrenArraysUnchangedWhenUnsuccessful() {

        // Array Children will remain the same
        // if deleteChild(Object) uses a wrong Object

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        Human[] children1 = new Human[family1.getChildren().length];

        for (int i = 0; i < children1.length; i++) {
            children1[i] = new Human(
                    family1.getChildren()[i].getName(),
                    family1.getChildren()[i].getSurname(),
                    family1.getChildren()[i].getYearBirthDay(),
                    family1.getChildren()[i].getIq(),
                    family1.getChildren()[i].getSchedule(),
                    family1.getChildren()[i].getFamily()
            );
        }

        family1.deleteChild(human1);

        Assert.assertEquals(family1.getChildren().length, 3);
        Assert.assertArrayEquals(children1, family1.getChildren());

    }

    @Test
    public void deleteChildByObjectReturnsFalseWhenUnsuccessful() {

        // deleteChild returns false
        // if deleteChild(Object) uses a wrong Object

        Family family1 = new Family(human1, human2);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);

        boolean result = family1.deleteChild(human1);

        Assert.assertFalse(result);

    }

    @Test
    public void addChildIncreasesChildrenArrayLengthByOneWhenSuccessful() {

        // Array Children increases length
        // after successful addChild(Object)

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(family1.getChildren().length, 0);

        family1.addChild(child1);
        Assert.assertEquals(family1.getChildren().length, 1);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1], child1);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1].getFamily(), family1);

        family1.addChild(child2);
        Assert.assertEquals(family1.getChildren().length, 2);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1], child2);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1].getFamily(), family1);

        family1.addChild(child3);
        Assert.assertEquals(family1.getChildren().length, 3);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1], child3);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1].getFamily(), family1);

    }

    @Test
    public void countFamilyIncreasesByOneWhenAddChildSuccessful() {

        Family family1 = new Family(human1, human2);

        Assert.assertEquals(family1.countFamily(), 2);

        family1.addChild(child1);

        Assert.assertEquals(family1.countFamily(), 3);

        family1.addChild(child2);

        Assert.assertEquals(family1.countFamily(), 4);

        family1.addChild(child3);

        Assert.assertEquals(family1.countFamily(), 5);

        family1.deleteChild(child3);

        Assert.assertEquals(family1.countFamily(), 4);

        family1.deleteChild(child3);

        Assert.assertEquals(family1.countFamily(), 4);

    }


}
